from setuptools import setup, find_packages
setup(
    name="PyCaosDB Integration Tests",
    version="0.1.0",
    packages=find_packages(),
    tests_require=["nose>=1.0"],
)
