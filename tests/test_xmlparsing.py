# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 16.09.2013.

@author: tf
"""


def test_parse_xml():
    from lxml import etree
    from nose.tools import assert_equals  # @UnresolvedImport
    from caosdb.common.models import File
    from caosdb.common.models import parse_xml
    from caosdb.common.models import Parent
    from caosdb.common.models import Property
    from caosdb.common.models import Message

    # define test file xml
    tmp_name = 'filename'
    tmp_id = 101
    tmp_desc = "description file"
    tmp_path = "path/to/file"
    tmp_checksum = "SHA512Checksum"
    tmp_size = '259342'
    tmp_type = "sqlite"
    elem = etree.Element("File")
    elem.set('name', str(tmp_name))
    elem.set("id", str(tmp_id))
    elem.set("description", str(tmp_desc))
    elem.set("path", str(tmp_path))
    elem.set("checksum", str(tmp_checksum))
    elem.set("size", str(tmp_size))
    elem.set("datatype", str(tmp_type))

    # define parent entity
    tmp_par_name = "parname"
    tmp_par_id = "102"
    tmp_par_desc = "par description"
    par = etree.Element("Parent")
    par.set("name", tmp_par_name)
    par.set("id", tmp_par_id)
    par.set("description", tmp_par_desc)
    elem.append(par)

    # define property
    tmp_prop_name = "propname"
    tmp_prop_id = "103"
    tmp_prop_desc = "prop desc"
    tmp_prop_unit = "meter"
    tmp_prop_exp = 9
    tmp_prop_value = 10
    tmp_prop_imp = "recommended"
    prop = etree.Element("Property")
    prop.set("id", str(tmp_prop_id))
    prop.set("importance", str(tmp_prop_imp))
    prop.set("name", str(tmp_prop_name))
    prop.set("description", str(tmp_prop_desc))
    prop.set("unit", str(tmp_prop_unit))
    prop.text = str(tmp_prop_value)
    elem.append(prop)

    # define message
    tmp_msg_body = "Everything was fine."
    tmp_msg_type = "Info"
    msg = etree.Element(tmp_msg_type)
    msg.text = tmp_msg_body
    elem.append(msg)

    xml = etree.tostring(elem, pretty_print=True)
    print("========= Testing =========")
    print(xml)
    filerec = parse_xml(xml)

    assert isinstance(filerec, File)
    assert_equals(filerec.name, tmp_name)
    assert_equals(filerec.id, tmp_id)
    assert_equals(filerec.description, tmp_desc)
    assert_equals(filerec.path, tmp_path)
    assert_equals(filerec._checksum, tmp_checksum)
    assert_equals(filerec._size, tmp_size)
    assert_equals(filerec.datatype, tmp_type)

    for par in filerec.get_parents():
        assert isinstance(par, Parent)
        assert_equals(par.name, tmp_par_name)
        assert_equals(par.id, int(tmp_par_id))
        assert_equals(par.description, tmp_par_desc)
    for prop in filerec.get_properties():
        assert isinstance(prop, Property)
        assert_equals(prop.name, tmp_prop_name)
        assert_equals(prop.id, int(tmp_prop_id))
        assert_equals(prop.description, tmp_prop_desc)
        assert_equals(prop.unit, tmp_prop_unit)
        assert_equals(prop.value, tmp_prop_value)
        assert_equals(filerec.get_importance(prop), tmp_prop_imp)
    for msg in filerec.get_messages():
        assert isinstance(msg, Message)
        assert_equals(msg.type.lower(), tmp_msg_type.lower())
        assert_equals(msg.body, tmp_msg_body)
