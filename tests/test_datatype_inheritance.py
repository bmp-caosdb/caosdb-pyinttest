# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
from caosdb import Container, Property, RecordType, Record, execute_query
import caosdb
import caosdb as db
from caosdb.connection.connection import get_connection
# @UnresolvedImport
from nose.tools import assert_is_not_none, assert_true, assert_equals, with_setup

from caosdb.exceptions import EntityError


def setup():
    teardown()


def teardown():
    try:
        db.execute_query("FIND test_*").delete()
    except BaseException:
        pass
    try:
        db.execute_query("FIND Test*").delete()
    except BaseException:
        pass


@with_setup(setup, teardown)
def test_default_datatype_for_recordtypes():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").add_property(name="TestRT1").insert()
    p = db.execute_query("FIND TestRT2", unique=True).get_properties()[0]
    assert_equals(p.id, rt1.id)
    assert_equals(p.name, rt1.name)
    assert_equals(p.datatype, rt1.name)

    p = rt2.get_properties()[0]
    assert_equals(p.id, rt1.id)
    assert_equals(p.name, rt1.name)
    assert_equals(p.datatype, rt1.name)


@with_setup(setup, teardown)
def test_datatype_inheritance():

    insert = '<Insert><Property id="-1" name="test_property" description="bla" datatype="Text"/><RecordType name="test_rt" description="bla">    <Property id="-1" importance="obligatory" /></RecordType></Insert>'

    print(insert)
    con = get_connection()
    http_response = con.insert(
        entity_uri_segment=["Entity"], body=insert)

    c = Container._response_to_entities(http_response)
    print(c)
    for e in c:
        assert_true(e.is_valid())
        assert_is_not_none(e.id)
        assert_true(int(e.id) >= 100)

    c.delete()


def test_datatype_overriding():
    try:
        p = Property(
            name="DatatypeOverridingDoubleProperty",
            description="DoubleDesc",
            datatype="DOUBLE").insert()
        assert_true(p.is_valid())

        rt = RecordType(
            name="DatatypeOverridingRT").add_property(
            p, datatype="TEXT")
        rt.insert()
        assert_true(rt.is_valid())
        assert_is_not_none(rt.get_properties())
        assert_equals(1, len(rt.get_properties()))
        assert_equals(
            "DatatypeOverridingDoubleProperty",
            rt.get_properties()[0].name)
        assert_equals("DoubleDesc", rt.get_properties()[0].description)
        assert_equals(
            str("TEXT").lower(),
            rt.get_properties()[0].datatype.lower())

        # retrieve again:
        rt = execute_query("FIND DatatypeOverridingRT", unique=True)
        assert_true(rt.is_valid())
        assert_is_not_none(rt.get_properties())
        assert_equals(1, len(rt.get_properties()))
        assert_equals(
            "DatatypeOverridingDoubleProperty",
            rt.get_properties()[0].name)
        assert_equals("DoubleDesc", rt.get_properties()[0].description)
        assert_equals(
            str("TEXT").lower(),
            rt.get_properties()[0].datatype.lower())
    finally:
        try:
            execute_query("FIND DatatypeOverriding*").delete()
        except BaseException:
            pass


def test_datatype_overriding_update():
    try:
        p = Property(name="DoubleProperty", datatype="DOUBLE").insert()
        assert_true(p.is_valid())

        rt = RecordType(
            name="DatatypeOverridingRT").add_property(
            p, datatype="TEXT")
        rt.insert()
        assert_true(rt.is_valid())
        assert_is_not_none(rt.get_properties())
        assert_equals(1, len(rt.get_properties()))
        assert_equals("DoubleProperty", rt.get_properties()[0].name)
        assert_equals(
            str("TEXT").lower(),
            rt.get_properties()[0].datatype.lower())

        # retrieve again:
        rt = execute_query("FIND DatatypeOverridingRT", unique=True)
        assert_true(rt.is_valid())
        assert_is_not_none(rt.get_properties())
        assert_equals(1, len(rt.get_properties()))
        assert_equals("DoubleProperty", rt.get_properties()[0].name)
        assert_equals(
            str("TEXT").lower(),
            rt.get_properties()[0].datatype.lower())

        try:
            p.datatype = "INT"
            p.update()
            raise AssertionError("This should raise an EntityError!")
        except EntityError as e:
            assert_equals("Unknown datatype.", e.msg)

        p.datatype = "INTEGER"
        p.update()
        assert_true(p.is_valid())
        assert_equals(str("INTEGER").lower(), p.datatype.lower())

        # retrieve again:
        p = execute_query("FIND DoubleProperty", unique=True)
        assert_true(p.is_valid())
        assert_equals(str("INTEGER").lower(), p.datatype.lower())

        # retrieve rt again:
        rt = execute_query("FIND DatatypeOverridingRT", unique=True)
        assert_true(rt.is_valid())
        assert_is_not_none(rt.get_properties())
        assert_equals(1, len(rt.get_properties()))
        assert_equals("DoubleProperty", rt.get_properties()[0].name)
        # overriding still ok?
        assert_equals(
            str("TEXT").lower(),
            rt.get_properties()[0].datatype.lower())

        rt.get_properties()[0].datatype = "DATETIME"
        rt.update()
        assert_true(rt.is_valid())
        assert_is_not_none(rt.get_properties())
        assert_equals(1, len(rt.get_properties()))
        assert_equals("DoubleProperty", rt.get_properties()[0].name)
        assert_equals(
            str("DATETIME").lower(),
            rt.get_properties()[0].datatype.lower())

    finally:
        try:
            if len(execute_query("FIND DatatypeOverridingRT")) > 0:
                rt.delete()
        except BaseException:
            pass
        try:
            p.delete()
        except BaseException:
            pass


def test_recordtype_to_record():
    try:
        rt = RecordType(name="SimpleTextRecordType")
        rt.datatype = "TEXT"
        rt.insert()
        assert_true(rt.is_valid())
        assert_is_not_none(rt.datatype)
        assert_equals(str("TEXT").lower(), rt.datatype.lower())

        rt = RecordType(id=rt.id).retrieve()
        assert_true(rt.is_valid())
        assert_is_not_none(rt.datatype)
        assert_equals(str("TEXT").lower(), rt.datatype.lower())

        rec = Record().add_parent(name="SimpleTextRecordType").insert()
        assert_true(rec.is_valid())
        # TODO
        # assert_is_not_none(rec.datatype)
        # assert_equals(str("TEXT").lower(),rec.datatype.lower())

    finally:
        try:
            rt = execute_query("FIND SimpleTextRecordType").delete()
        except BaseException:
            pass


def test_concrete_property():
    def test(datatype):
        try:
            p = Property(
                name="DataTypeTestDoubleProperty",
                datatype=datatype.upper()).insert()
            assert_true(p.is_valid())
            assert_equals(p.datatype.upper(), datatype.upper())

            p2 = Property(id=p.id).retrieve()
            assert_equals(p.datatype, p2.datatype)

            return p
        finally:
            try:
                p.delete()
            except BaseException:
                pass
    try:
        p_double = test("DOUBLE")
        p_datetime = test("DATETIME")
        p_integer = test("INTEGER")
        p_timespan = test("TIMESPAN")
        p_text = test("TEXT")
        p_file = test("FILE")
        p_reference = test("REFERENCE")

    finally:
        try:
            p_reference.delete()
        except BaseException:
            pass
        try:
            p_file.delete()
        except BaseException:
            pass
        try:
            p_text.delete()
        except BaseException:
            pass
        try:
            p_timespan.delete()
        except BaseException:
            pass
        try:
            p_integer.delete()
        except BaseException:
            pass
        try:
            p_datetime.delete()
        except BaseException:
            pass
        try:
            p_double.delete()
        except BaseException:
            pass


def test_reference_with_null_value():
    try:
        try:
            execute_query("FIND RT2").delete()
        except BaseException:
            pass
        try:
            execute_query("FIND RT1").delete()
        except BaseException:
            pass
        RT1 = RecordType(name="RT1").insert()
        assert_true(RT1.is_valid())

        RT2 = RecordType(name="RT2").add_property(RT1).insert()
        assert_true(RT2.is_valid())
        assert_equals(RT2.get_property("RT1").datatype, "RT1")

        RT2c = RecordType(name="RT2").retrieve()
        assert_true(RT2c.is_valid())
        assert_equals(RT2c.get_property("RT1").datatype, "RT1")

    finally:
        try:
            RT2.delete()
        except BaseException:
            pass
        try:
            RT1.delete()
        except BaseException:
            pass


def test_duplicate_properties():
    try:
        p = Property(
            name="SimpleTestProperty",
            datatype=caosdb.DOUBLE).insert()
        assert_true(p.is_valid())

        rt = RecordType(
            name="SimpleTestRecordType").add_property(
            p,
            datatype=caosdb.TEXT).add_property(
            p,
            datatype=caosdb.INTEGER).insert()
        assert_true(rt.is_valid())
    finally:
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            p.delete()
        except BaseException:
            pass
