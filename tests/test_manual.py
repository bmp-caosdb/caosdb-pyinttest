# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
from caosdb import Record, RecordType, Property, Container, File, execute_query

# @UnresolvedImport
from nose.tools import assert_not_equals, assert_true, assert_is_not_none, assert_equals
from nose.tools.nontrivial import nottest


def test_retrieve_shortcuts():
    c = Container().retrieve(
        [Record(1337), Record(1338)], raise_exception_on_error=False)
    assert_is_not_none(c.get_entity_by_id(1337))
    assert_is_not_none(c.get_entity_by_id(1338))

    c = Container().retrieve([1337, 1338], raise_exception_on_error=False)
    assert_is_not_none(c.get_entity_by_id(1337))
    assert_is_not_none(c.get_entity_by_id(1338))

    c = Container().retrieve(
        [RecordType("fridge temperature experiment"),
         Property("date")],
        raise_exception_on_error=False)
    assert_is_not_none(c.get_entity_by_name("fridge temperature experiment"))
    assert_is_not_none(c.get_entity_by_name("date"))


def test_references():
    try:
        """define first name, last name, and person."""
        firstname = Property(
            id=-2,
            name="TestFirst Name",
            description="The first name of a Person.",
            datatype="text")
        lastname = Property(
            id=-3,
            name="TestLast Name",
            description="The last name of a Person.",
            datatype="text")
        person = RecordType(name="TestPerson", description="A person.")
        person.add_property(firstname, importance="obligatory")
        person.add_property(lastname, importance="obligatory")

        '''define conductor'''
        conductor = Property(
            name="TestConductor",
            description="The guy who conducted an experiment.",
            datatype=person)

        '''define experiment'''
        experiment = RecordType(name="TestExperiment")
        experiment.add_property(conductor, importance="obligatory")

        '''define Mr./Mrs. Who Cares'''
        whocares = Record(id=-1, name="TestWhoCares")
        whocares.add_parent(person)
        whocares.add_property(firstname, value="Who")
        whocares.add_property(lastname, value="Cares")
        # print(whocares)

        '''define particular experiment'''
        exp1 = Record("TestExperimentRecord")
        exp1.add_parent(experiment)
        exp1.add_property(conductor, value=whocares)
        # print(exp1.get_properties()[0])
        # print(exp1)

        '''add to a container and insert everything'''
        container = Container()
        container.extend([firstname, lastname, person,
                          conductor, experiment, whocares, exp1])
        container.insert()

        print(container)

        container.delete()
    finally:
        try:
            execute_query("FIND Test*").delete()
        except BaseException:
            pass


@nottest
def test_name_overriding():
    from caosdb import execute_query

    try:
        # RecordType "Person"
        person = RecordType(name="PersonTestManual").insert()
        assert_true(person.is_valid())
        assert_equals("PersonTestManual", person.name)

        # RecordType "Experiment" with Property "Person" but overridden name
        # "Conductor"
        experiment = RecordType(
            name="ExperimentTestManual").add_property(
            property=person,
            name="ConductorTestManual",
            description="The person who conducted this experiment.").insert()
        assert_true(experiment.is_valid())
        assert_is_not_none(experiment.get_properties())
        assert_equals(1, len(experiment.get_properties()))
        assert_is_not_none(experiment.get_properties()[0])
        assert_equals(person.id, experiment.get_properties()[0].id)
        assert_not_equals(person.name, experiment.get_properties()[0].name)
        assert_equals(
            "ConductorTestManual",
            experiment.get_properties()[0].name)

        # Retrieve again
        c_experiment = RecordType(name="ExperimentTestManual").retrieve()
        assert_true(c_experiment.is_valid())
        assert_is_not_none(c_experiment.get_properties())
        assert_equals(1, len(c_experiment.get_properties()))
        assert_is_not_none(c_experiment.get_properties()[0])
        assert_equals(person.id, c_experiment.get_properties()[0].id)
        assert_not_equals(person.name, c_experiment.get_properties()[0].name)
        assert_equals(
            "ConductorTestManual",
            c_experiment.get_properties()[0].name)

        # Instantiate "Person" as Record "Timm"
        sp_person = Record(name="Peter").add_parent(person).insert()
        assert_true(sp_person.is_valid())

        # Instantiate "Experiment" with "Timm" as Conductor. Just add property
        # via id.
        sp_experiment_1 = Record().add_parent(
            id=experiment.id).add_property(
            id=person.id, value=sp_person).insert()
        assert_true(sp_experiment_1.is_valid())

        # Does it recognize "Person" as "Condutor"?
        assert_is_not_none(sp_experiment_1.get_properties())
        assert_equals(1, len(sp_experiment_1.get_properties()))
        assert_is_not_none(sp_experiment_1.get_properties()[0])
        assert_equals(person.id, sp_experiment_1.get_properties()[0].id)
        assert_not_equals(
            person.name,
            sp_experiment_1.get_properties()[0].name)
        assert_equals(
            "ConductorTestManual",
            sp_experiment_1.get_properties()[0].name)

        # Retrieve again
        c_sp_experiment_1 = Record(id=sp_experiment_1.id).retrieve()
        assert_true(c_sp_experiment_1.is_valid())
        assert_is_not_none(c_sp_experiment_1.get_properties())
        assert_equals(1, len(c_sp_experiment_1.get_properties()))
        assert_is_not_none(c_sp_experiment_1.get_properties()[0])
        assert_equals(person.id, c_sp_experiment_1.get_properties()[0].id)
        assert_not_equals(
            person.name,
            c_sp_experiment_1.get_properties()[0].name)
        assert_equals(
            "ConductorTestManual",
            c_sp_experiment_1.get_properties()[0].name)

        # Query for it:
        c2_sp_experiment_1 = execute_query(
            "FIND RECORD . ConductorTestManual=Peter", unique=True)
        assert_true(c2_sp_experiment_1.is_valid())
        c2_sp_experiment_1 = execute_query(
            "FIND RECORD . PersonTestManual=Peter", unique=True)
        assert_true(c2_sp_experiment_1.is_valid())

        c2_sp_experiment_1 = execute_query(
            "FIND RECORD . ConductorTestManual -> Peter", unique=True)
        assert_true(c2_sp_experiment_1.is_valid())
        c2_sp_experiment_1 = execute_query(
            "FIND RECORD . PersonTestManual -> Peter", unique=True)
        assert_true(c2_sp_experiment_1.is_valid())

    finally:
        pass
        if len(execute_query("FIND ExperimentTestManual")) > 1:
            sp_experiment_1.delete()
        if len(execute_query("FIND PersonTestManual")) > 1:
            sp_person.delete()
        if len(execute_query("FIND ExperimentTestManual")) > 0:
            experiment.delete()
        person.delete()
