# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on Jan 20, 2015.

@author: fitschen
"""
from nose.tools import assert_equals, assert_is_not_none, assert_true  # @UnresolvedImport
import caosdb as h
import os
from caosdb.exceptions import CaosDBException


'''1) Struktur:
1.1 Einfügen von einfachen RecordTypes, die nur "basic" Properties
enthalten (Gewicht, Beschreibung, Identifkationsnummern, etc.)
1.2 Einfügen von RecordTypes, die zusätzlich Dateien referenzieren
1.3 Einfügen von komplexen RecordTypes, die zusätzlich Referenzen auf
andere RecordTypes enthalten
1.4 Verwendung von Vererbung bei RecordTypes (mit / ohne Kopieren von
Eigenschaften zum Kind)

1.5 Einfügen von RecordTypes unter Verwendung schon vorhandener
RecordTypes / Properties

2) Daten:
2.1 Einfügen von Datensätzen unter Benutzung der oben angelegten RecordTypes
2.2 Hochladen von Dateien über HTTP und DropOffBox und Verknüpfung mit den
Datensätzen
2.3 Verwendung von Einheiten
2.4 Nachtägliches Annotieren von Datensätzen, z.B. hinzufügen einer
Description oder einer Zahl'''


def setup_module():
    try:
        h.execute_query("FIND Complex*").delete()
    except Exception as e:
        print(e)
    try:
        h.execute_query("FIND ReferenceProperty").delete()
    except Exception as e:
        print(e)
    try:
        h.execute_query("FIND Simple*").delete()
    except Exception as e:
        print(e)


def teardown_module():
    setup_module()


def test_tenpoints1_1():
    '''
    1.1 Einfügen von einfachen RecordTypes, die nur "basic" Properties
      enthalten (Gewicht, Beschreibung, Identifkationsnummern, etc.)
    '''

    try:
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_tenpoints.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_tenpoints.py)",
                datatype='double'))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_tenpoints.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_tenpoints.py)",
                datatype='datetime'))

        c.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_tenpoints.py)") .add_property(
                name='SimpleTextProperty') .add_property(
                name='SimpleDoubleProperty') .add_property(
                    name='SimpleIntegerProperty') .add_property(
                        name='SimpleDatetimeProperty'))

        c.insert()
    finally:
        try:
            c.delete()
        except BaseException:
            pass


def test_tenpoints1_2():
    '''
    1.2 Einfügen von RecordTypes, die zusätzlich Dateien referenzieren
    '''
    try:
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_tenpoints.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_tenpoints.py)",
                datatype='double'))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_tenpoints.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_tenpoints.py)",
                datatype='datetime'))
        c.append(
            h.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_tenpoints.py)",
                datatype='file'))

        c.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_tenpoints.py)") .add_property(
                name='SimpleTextProperty') .add_property(
                name='SimpleDoubleProperty') .add_property(
                    name='SimpleIntegerProperty') .add_property(
                        name='SimpleDatetimeProperty') .add_property(
                            name='SimpleFileProperty'))

        c.insert()
    finally:
        try:
            c.delete()
        except BaseException:
            pass


def test_tenpoints1_3():
    '''
    1.3 Einfügen von komplexen RecordTypes, die zusätzlich Referenzen auf
      andere RecordTypes enthalten
    '''

    try:
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_tenpoints.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_tenpoints.py)",
                datatype='double'))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_tenpoints.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_tenpoints.py)",
                datatype='datetime'))
        c.append(
            h.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_tenpoints.py)",
                datatype='file'))

        c.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_tenpoints.py)") .add_property(
                name='SimpleTextProperty') .add_property(
                name='SimpleDoubleProperty') .add_property(
                    name='SimpleIntegerProperty') .add_property(
                        name='SimpleDatetimeProperty') .add_property(
                            name='SimpleFileProperty'))

        c.append(
            h.Property(
                name="ReferenceProperty",
                description='reference property (from test_tenpoints.py)',
                datatype="SimpleRecordType"))
        c.append(h.RecordType(name="ComlexRecordType", description="complex recordType with references (from test_tenpoints.py)")
                 .add_property(name='ReferenceProperty')  # first method
                 # second method, doesn't need the ReferenceProperty
                 .add_property(name='SimpleRecordType')
                 )

        c.insert()
    finally:
        try:
            c.delete()
        except BaseException:
            pass


def test_tenpoints1_4():
    '''
    1.4 Verwendung von Vererbung bei RecordTypes (mit / ohne Kopieren von
      Eigenschaften zum Kind)
    '''

    print("see ./test_inheritance.py")


def test_tenpoints1_5():
    '''
    1.5 Einfügen von RecordTypes unter Verwendung schon vorhandener
      RecordTypes / Properties
    '''

    try:
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_tenpoints.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_tenpoints.py)",
                datatype='double'))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_tenpoints.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_tenpoints.py)",
                datatype='datetime'))
        c.append(
            h.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_tenpoints.py)",
                datatype='file'))
        c.insert()

        d = h.Container()
        d.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_tenpoints.py)") .add_property(
                name='SimpleTextProperty') .add_property(
                name='SimpleDoubleProperty') .add_property(
                    name='SimpleIntegerProperty') .add_property(
                        name='SimpleDatetimeProperty') .add_property(
                            name='SimpleFileProperty'))

        d.append(
            h.Property(
                name="ReferenceProperty",
                description='reference property (from test_tenpoints.py)',
                datatype="SimpleRecordType"))
        d.append(h.RecordType(name="ComplexRecordType", description="complex recordType with references (from test_tenpoints.py)")
                 .add_property(name='ReferenceProperty')  # first method
                 # second method, doesn't need the ReferenceProperty
                 .add_property(name='SimpleRecordType')
                 )

        d.insert()

        sr = (h.Record().add_parent(name="SimpleRecordType")
              .add_property(name="SimpleTextProperty", value="Some Text")
              .add_property(name="SimpleDoubleProperty", value=3.14)
              .add_property(name="SimpleIntegerProperty", value=1337)
              )

        sr.insert()

        cr1 = (h.Record().add_parent(name="ComplexRecordType")
               .add_property(name="ReferenceProperty", value=sr))

        cr1.insert()

        cr2 = (h.Record().add_parent(name="ComplexRecordType")
               .add_property(name="SimpleRecordType", value=sr))

        cr2.insert()

    finally:
        try:
            cr2.delete()
        except BaseException:
            pass
        try:
            cr1.delete()
        except BaseException:
            pass
        try:
            sr.delete()
        except BaseException:
            pass
        try:
            d.delete()
        except BaseException:
            pass
        try:
            c.delete()
        except BaseException:
            pass


def test_tenpoints2_1AND2_2():
    '''
    2.1 Einfügen von Datensätzen unter Benutzung der oben angelegten RecordTypes
    & 2.2 Hochladen von Dateien über HTTP und DropOffBox und Verknüpfung mit den
      Datensätzen
    '''

    try:
        """data model."""
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_tenpoints.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_tenpoints.py)",
                datatype='double',
                unit="m"))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_tenpoints.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_tenpoints.py)",
                datatype='datetime'))
        c.append(
            h.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_tenpoints.py)",
                datatype='file'))

        c.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_tenpoints.py)") .add_property(
                name='SimpleTextProperty',
                importance='obligatory') .add_property(
                name='SimpleDoubleProperty',
                importance='obligatory') .add_property(
                    name='SimpleIntegerProperty',
                    importance='obligatory') .add_property(
                        name='SimpleDatetimeProperty',
                        importance='obligatory') .add_property(
                            name='SimpleFileProperty',
                importance='obligatory'))

        c.append(
            h.Property(
                name="ReferenceProperty",
                description='reference property (from test_tenpoints.py)',
                datatype="SimpleRecordType"))
        c.append(h.RecordType(name="ComplexRecordType", description="complex recordType with references (from test_tenpoints.py)")
                 .add_property(name='ReferenceProperty')  # first method
                 # second method, doesn't need the ReferenceProperty
                 .add_property(name='SimpleRecordType')
                 )

        c.insert()
        print(c)

        ''' data'''
        d = h.Container()

        ''' create testfile '''
        f = open("test.dat", "w")
        f.write("hello world\n")
        f.close()

        ''' prepare file record '''
        d.append(
            h.File(
                id=-1,
                path='testfiles/testfile',
                file="test.dat",
                name="SimpleFileRecord"))

        ''' prepare simple record with file property'''
        d.append(
            h.Record(
                id=-2,
                name="SimpleRecord") .add_parent(
                name='SimpleRecordType') .add_property(
                name='SimpleTextProperty',
                value='some text') .add_property(
                    name='SimpleDoubleProperty',
                    value=3.14) .add_property(
                        name='SimpleIntegerProperty',
                        value=1337) .add_property(
                            name='SimpleDatetimeProperty',
                            value='2015-12-24 18:15:00') .add_property(
                                name='SimpleFileProperty',
                value=-1))

        d.append(h.Record()
                 .add_parent(name="ComplexRecordType")
                 .add_property(name="ReferenceProperty", value=-2)
                 )

        d.insert()

        e = d.get_entity_by_name("SimpleRecord")
        assert_is_not_none(e)
        assert_true(e.is_valid())
        fp = e.get_property("SimpleFileProperty")
        assert_is_not_none(fp)
        assert_is_not_none(fp.value)

        fr = d.get_entity_by_name("SimpleFileRecord")
        assert_is_not_none(fr)
        assert_true(fr.is_valid())
        assert_is_not_none(fr.id)

        assert_equals(int(fp.value), fr.id)
        print(d)
    finally:
        try:
            d.delete()
        except BaseException:
            pass
        try:
            c.delete()
        except BaseException:
            pass
        try:
            os.remove("test.dat")
        except BaseException:
            pass

    try:
        """insert file via dropOffBox."""
        dob = h.DropOffBox().sync()
        if os.path.isdir(dob.path):
            f = open("test.dat", "w")
            f.write("hello world\n")
            f.close()
            import shutil
            shutil.move("test.dat", dob.path)
            fo = h.File(path='testfiles/testfile2', pickup="test.dat").insert()
    finally:
        try:
            fo.delete()
        except BaseException:
            pass


def test_tenpoints2_3():
    '''2.3 Verwendung von Einheiten'''

    try:
        """insert simple property with unit."""
        p1 = h.Property(name='unittestproperty', datatype='double', unit='m')
        p1.insert()

        '''subtyping with unit inheritance'''
        p2 = h.Property(name='SubTypeOfUnitTestProperty').add_parent(id=p1.id)
        p2.insert()
        assert_equals('m', p2.unit)

        '''implement property'''
        rt = h.RecordType(
            name='SimpleRecordType',
            description='simple recordType (from test_tenpoints.py)').add_property(
            id=p2.id)
        rt.insert()
        print(rt)
        rtid = rt.id
        rt = h.RecordType(id=rtid).retrieve()
        print(rt)
        assert_equals(p2.id, rt.get_properties()[0].id)

        assert_equals('m', rt.get_properties()[0].unit)
    finally:
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            p2.delete()
        except BaseException:
            pass
        try:
            p1.delete()
        except BaseException:
            pass


def test_tenpoints2_4():
    '''2.4 Nachtägliches Annotieren von Datensätzen, z.B. hinzufügen einer
         Description oder einer Zahl'''

    try:
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_tenpoints.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_tenpoints.py)",
                datatype='double'))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_tenpoints.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_tenpoints.py)",
                datatype='datetime'))
        c.append(
            h.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_tenpoints.py)",
                datatype='file'))
        c.append(
            h.Property(
                name="CommentProperty",
                description="comment property",
                datatype='text'))
        c.insert()

        d = h.Container()

        d.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_tenpoints.py)") .add_property(
                name='SimpleTextProperty',
                importance='obligatory') .add_property(
                name='SimpleDoubleProperty',
                importance='obligatory') .add_property(
                    name='SimpleIntegerProperty',
                    importance='obligatory') .add_property(
                        name='SimpleDatetimeProperty') .add_property(
                            name='SimpleFileProperty'))

        d.append(
            h.Property(
                name="ReferenceProperty",
                description='reference property (from test_tenpoints.py)',
                datatype="SimpleRecordType"))
        d.append(h.RecordType(name="ComplexRecordType", description="complex recordType with references (from test_tenpoints.py)")
                 .add_property(name='ReferenceProperty')  # first method
                 # second method, doesn't need the ReferenceProperty
                 .add_property(name='SimpleRecordType')
                 )

        d.insert()

        ''' create testfile '''
        f = open("test.dat", "w")
        f.write("hello world\n")
        f.close()

        ''' prepare file record '''
        e = h.Container()
        e.append(h.File(id=-1, path='testfiles/testfile', file="test.dat"))

        sr = (h.Record().add_parent(name="SimpleRecordType")
              .add_property(name="SimpleTextProperty", value="Some Text")
              .add_property(name="SimpleDoubleProperty", value=3.14)
              .add_property(name="SimpleIntegerProperty", value=1337)
              .add_property(name='SimpleFileProperty', value=-1)
              )

        e.append(sr).insert()

        cr1 = (
            h.Record(
                name="My special unique record name").add_parent(
                name="ComplexRecordType") .add_property(
                name="ReferenceProperty",
                value=sr))

        cr1.insert()

        cr2 = (h.Record().add_parent(name="ComplexRecordType")
               .add_property(name="SimpleRecordType", value=sr))

        recid = cr2.insert().id

        '''annotate file'''
        h.File(
            path='testfiles/testfile').retrieve().add_property(
            c.get_entity_by_name("CommentProperty"),
            value="This is a really nice file.").update()

        '''annotate records'''
        h.Record(
            name="My special unique record name").retrieve().add_property(
            c.get_entity_by_name("CommentProperty"),
            value="This is a really nice record.").update()
        h.Record(
            id=recid).retrieve().add_property(
            c.get_entity_by_name("CommentProperty"),
            value="I don't like this record.").update()
        h.execute_query(
            "FIND RECORD SimpleRecordType WITH SimpleIntegerProperty=1337 AND SimpleTextProperty='Some Text'",
            unique=True).add_property(
            c.get_entity_by_name("CommentProperty"),
            value="I looove this record!!!!!11.").update()

    finally:
        try:
            cr2.delete()
        except BaseException:
            pass
        try:
            cr1.delete()
        except BaseException:
            pass
        try:
            e.delete()
        except BaseException:
            pass
        try:
            d.delete()
        except BaseException:
            pass
        try:
            c.delete()
        except BaseException:
            pass


def test_existence():

    try:
        h.Property(name="SimpleTextProperty").retrieve(unique=True)

    except CaosDBException:
        print("SimpleTextProperty does not exist!")

    p = h.Property(
        name="SimpleTextProperty",
        description="simple text property (from test_tenpoints.py)",
        datatype='text').insert()

    if int(
        h.Property(
            name="SimpleTextProperty").retrieve(
            unique=True).id) >= 0:
        print("SimpleTextProperty does exist!")

    p.delete()


def test_cleanup():
    try:
        h.execute_query("FIND Simple*").delete()
    except BaseException:
        pass
