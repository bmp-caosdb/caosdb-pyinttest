# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 21.01.2015.

@author: tf
"""


import caosdb as h
from nose.tools import assert_equals, assert_true, assert_is_not_none, with_setup


def test_inheritance_fix_properties():
    try:
        """FIX PROPERTIES."""
        '''insert simple property with unit'''
        p1 = h.Property(name='UnitTestProperty', datatype='double', unit='m')
        p1.insert()

        p1c = h.Property(id=p1.id).retrieve()
        assert_true(p1c.is_valid())
        assert_equals('m', p1c.unit)

        '''subtyping with unit inheritance'''
        p2 = h.Property(
            name='SubTypeOfUnitTestProperty').add_parent(
            id=p1.id, inheritance="FIX")
        print(p2)
        p2.insert()
        print(p2)
        assert_true(p2.is_valid())
        assert_equals('m', p2.unit)
    finally:
        try:
            p2.delete()
        except BaseException:
            pass
        try:
            p1.delete()
        except BaseException:
            pass


def test_inheritance_obl_properties():
    try:
        """OBLIGATORY PROPERTIES."""
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_inheritance.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_inheritance.py)",
                datatype='double'))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_inheritance.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_inheritance.py)",
                datatype='datetime'))
        c.append(
            h.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_inheritance.py)",
                datatype='file'))

        c.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_inheritance.py)") .add_property(
                name='SimpleTextProperty',
                importance="obligatory") .add_property(
                name='SimpleDoubleProperty',
                importance="obligatory") .add_property(
                    name='SimpleIntegerProperty',
                    importance="obligatory") .add_property(
                        name='SimpleDatetimeProperty') .add_property(
                            name='SimpleFileProperty'))
        c.insert()

        rt = h.RecordType(
            name="SubTypeOfSimpleRecordType",
            description="recordtype with inheritance (from test_inheritance.py)").add_parent(
            name="SimpleRecordType",
            inheritance="obligatory")
        rt.insert()
        assert_equals(3, len(rt.get_properties()))
    finally:
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            c.delete()
        except BaseException:
            pass


def test_inheritance_all_properties():
    try:
        """ALL PROPERTIES."""
        c = h.Container()
        c.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_inheritance.py)",
                datatype='text'))
        c.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_inheritance.py)",
                datatype='double'))
        c.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_inheritance.py)",
                datatype='integer'))
        c.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_inheritance.py)",
                datatype='datetime'))
        c.append(
            h.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_inheritance.py)",
                datatype='file'))

        c.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_inheritance.py)") .add_property(
                name='SimpleTextProperty',
                importance="obligatory") .add_property(
                name='SimpleDoubleProperty',
                importance="obligatory") .add_property(
                    name='SimpleIntegerProperty',
                    importance="obligatory") .add_property(
                        name='SimpleDatetimeProperty') .add_property(
                            name='SimpleFileProperty'))
        c.insert()

        rt = h.RecordType(
            name="SubTypeOfSimpleRecordType",
            description="recordtype with inheritance (from test_inheritance.py)").add_parent(
            name="SimpleRecordType",
            inheritance="obligatory")
        rt.insert()
        assert_equals(3, len(rt.get_properties()))
    finally:
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            c.delete()
        except BaseException:
            pass


def test_inheritance_unit():
    try:

        p = h.Property(
            name="SimpleIntProperty",
            datatype="INTEGER",
            unit="m").insert()
        assert_true(p.is_valid())
        assert_equals("m", p.unit)

        rt = h.RecordType(
            name="SimpleRecordType").add_property(
            p, unit="km").insert()
        assert_true(rt.is_valid())
        assert_equals("km", rt.get_property("SimpleIntProperty").unit)

        rt2 = h.execute_query("FIND SimpleRecordType", True)
        assert_true(rt2.is_valid())
        assert_equals(rt2.id, rt.id)
        assert_equals(rt2.get_property("SimpleIntProperty").unit, "km")

        rt3 = h.RecordType(
            name="SimpleRecordType2").add_parent(
            rt, inheritance="ALL").insert()
        assert_true(rt3.is_valid())
        assert_is_not_none(rt3.get_property("SimpleIntProperty"))
        assert_equals(rt3.get_property("SimpleIntProperty").unit, "km")

        rt4 = h.execute_query("FIND SimpleRecordType2", True)
        assert_true(rt4.is_valid())
        assert_equals(rt4.id, rt3.id)
        assert_equals(rt4.get_property("SimpleIntProperty").unit, "km")

        rec = h.Record(
            name="SimpleRecord").add_parent(rt3).add_property(
            name="SimpleIntProperty",
            value=1).insert()
        assert_true(rec.is_valid())
        assert_is_not_none(rec.get_property("SimpleIntProperty"))
        assert_equals(rec.get_property("SimpleIntProperty").unit, "km")

    finally:
        try:
            rec.delete()
        except BaseException:
            pass
        try:
            rt3.delete()
        except BaseException:
            pass
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            p.delete()
        except BaseException:
            pass


_ENTITIES = [
    h.RecordType(name="Simulation").add_property(name="SimulationModel"),

    h.RecordType(name="PublicationReference").add_property(name="date"),
    h.RecordType(
        name="SimulationModel").add_property(
        name="PublicationReference"),

    h.Property(name="date", datatype=h.TEXT),
    h.Property(name="blub", datatype=h.TEXT),
]


def setup_subproperties():
    con = h.Container().extend(_ENTITIES)
    con.insert()


def setup_module():
    old = h.execute_query("FIND ENTITY WITH ID > 100")
    if old:
        old.delete()


def teardown_module():
    setup_module()


@with_setup(setup_subproperties)
def test_inherit_subproperties():
    valid = h.Container().extend(_ENTITIES).retrieve()
    container = h.Container().extend(_ENTITIES)
    container.get_entity_by_name("Simulation").add_property(name="blub")
    for valid_e in valid:
        for entity in container:
            for prop in entity.get_properties():
                if valid_e.name == prop.name:
                    prop.id = valid_e.id
                    entity.get_properties()._inheritance[prop] = h.ALL
    container.get_entity_by_name("SimulationModel").update()
    container.get_entity_by_name("Simulation").update()
