# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 27.02.2017.

@author: tf
"""

from caosdb import administration as admin
# @UnresolvedImport
from nose.tools import assert_true, assert_equals, assert_is_not_none, with_setup, assert_raises
from caosdb.exceptions import (ClientErrorException, TransactionError,
                               AuthorizationException, LoginFailedException)
from caosdb.connection.connection import get_connection, configure_connection

test_role = "test_role"
test_user = "test_user"
test_pw = "secret1P!"
test_role_desc = "This is a test role."


def setup_module():
    teardown_module()
    admin._insert_user(
        name=test_user,
        password=test_pw,
        status="ACTIVE",
        email=None,
        entity=None)


def teardown_module():
    switch_to_admin_user()
    try:
        admin._delete_user(name=test_user)
    except Exception as e:
        print(e)


def setup():
    switch_to_admin_user()


def teardown():
    switch_to_admin_user()
    try:
        admin._delete_user(name=test_user + "2")
    except Exception as e:
        print(e)
    try:
        admin._delete_role(name=test_role)
    except Exception as e:
        print(e)


def switch_to_normal_user():
    configure_connection(username=test_user, password=test_pw)


def switch_to_admin_user():
    configure_connection()


@with_setup(setup, teardown)
def test_insert_role_success():
    assert_true(admin._insert_role(name=test_role, description=test_role_desc))


@with_setup(setup, teardown)
def test_insert_role_failure_permission():
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._insert_role(name=test_role, description=test_role_desc)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to insert a new role.")


@with_setup(setup, teardown)
def test_insert_role_failure_name_duplicates():
    test_insert_role_success()
    with assert_raises(ClientErrorException) as cm:
        admin._insert_role(name=test_role, description=test_role_desc)
    assert_equals(
        cm.exception.msg,
        "Role name is already in use. Choose a different name.")


@with_setup(setup, teardown)
def test_update_role_success():
    test_insert_role_success()
    assert_is_not_none(
        admin._update_role(
            name=test_role,
            description=test_role_desc +
            "asdf"))


@with_setup(setup, teardown)
def test_update_role_failure_permissions():
    test_insert_role_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._update_role(name=test_role, description=test_role_desc + "asdf")
    assert_equals(
        cm.exception.msg,
        "You are not permitted to update this role.")


@with_setup(setup, teardown)
def test_update_role_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._update_role(name=test_role, description=test_role_desc + "asdf")
    assert_equals(cm.exception.msg, "Role does not exist.")


@with_setup(setup, teardown)
def test_delete_role_success():
    test_insert_role_success()
    assert_true(admin._delete_role(name=test_role))


@with_setup(setup, teardown)
def test_delete_role_failure_permissions():
    test_insert_role_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._delete_role(name=test_role)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to delete this role.")


@with_setup(setup, teardown)
def test_delete_role_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._delete_role(name=test_role)
    assert_equals(cm.exception.msg, "Role does not exist.")


@with_setup(setup, teardown)
def test_retrieve_role_success():
    test_insert_role_success()
    r = admin._retrieve_role(test_role)
    assert_is_not_none(r)


@with_setup(setup, teardown)
def test_retrieve_role_failure_permission():
    test_insert_role_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._retrieve_role(name=test_role)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to retrieve this role.")


@with_setup(setup, teardown)
def test_retrieve_role_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._retrieve_role(name=test_role)
    assert_equals(cm.exception.msg, "Role does not exist.")


@with_setup(setup, teardown)
def test_set_permissions_success():
    test_insert_role_success()
    assert_true(
        admin._set_permissions(
            role=test_role,
            permission_rules=[
                admin.PermissionRule(
                    "Grant",
                    "BLA:BLA:BLA")]))


@with_setup(setup, teardown)
def test_set_permissions_failure_permissions():
    test_insert_role_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._set_permissions(
            role=test_role, permission_rules=[
                admin.PermissionRule(
                    "Grant", "BLA:BLA:BLA")])
    assert_equals(
        cm.exception.msg,
        "You are not permitted to set this role's permissions.")


@with_setup(setup, teardown)
def test_set_permissions_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._set_permissions(
            role=test_role, permission_rules=[
                admin.PermissionRule(
                    "Grant", "BLA:BLA:BLA")])
    assert_equals(cm.exception.msg, "Role does not exist.")


@with_setup(setup, teardown)
def test_get_permissions_success():
    test_set_permissions_success()
    r = admin._get_permissions(role=test_role)
    assert_equals({admin.PermissionRule("Grant", "BLA:BLA:BLA")}, r)
    assert_is_not_none(r)


@with_setup(setup, teardown)
def test_get_permissions_failure_permissions():
    test_set_permissions_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._get_permissions(role=test_role)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to retrieve this role's permissions.")


@with_setup(setup, teardown)
def test_get_permissions_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._get_permissions(role="non-existing-role")
    assert_equals(cm.exception.msg, "Role does not exist.")


@with_setup(setup, teardown)
def test_get_roles_success():
    test_insert_role_success()
    r = admin._get_roles(username=test_user)
    assert_is_not_none(r)
    return r


@with_setup(setup, teardown)
def test_get_roles_failure_permissions():
    test_insert_role_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._get_roles(username=test_user)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to retrieve this user's roles.")


@with_setup(setup, teardown)
def test_get_roles_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._get_roles(username="non-existing-user")
    assert_equals(cm.exception.msg, "User does not exist.")


@with_setup(setup, teardown)
def test_set_roles_success():
    roles_old = test_get_roles_success()
    roles = {test_role}
    roles.union(roles_old)
    assert_is_not_none(admin._set_roles(username=test_user, roles=roles_old))
    assert_is_not_none(admin._set_roles(username=test_user, roles=roles))
    assert_is_not_none(admin._set_roles(username=test_user, roles=roles_old))


@with_setup(setup, teardown)
def test_set_roles_success_with_warning():
    test_insert_role_success()
    roles = {test_role}
    r = admin._set_roles(username=test_user, roles=roles)
    assert_is_not_none(r)
    assert_is_not_none(admin._set_roles(username=test_user, roles=[]))


@with_setup(setup, teardown)
def test_set_roles_failure_permissions():
    roles_old = test_get_roles_success()
    roles = {test_role}
    roles.union(roles_old)
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._set_roles(username=test_user, roles=roles_old)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to set this user's roles.")


def test_set_roles_failure_non_existing_role():
    roles = {"non-existing-role"}
    with assert_raises(ClientErrorException) as cm:
        admin._set_roles(username=test_user, roles=roles)
    assert_equals(cm.exception.msg, "Role does not exist.")


def test_set_roles_failure_non_existing_user():
    test_insert_role_success()
    roles = {test_role}
    with assert_raises(TransactionError) as cm:
        admin._set_roles(username="non-existing-user", roles=roles)
    assert_equals(cm.exception.msg, "User does not exist.")


@with_setup(setup, teardown)
def test_insert_user_success():
    admin._insert_user(
        name=test_user + "2",
        password="secret1P!",
        status="ACTIVE",
        email="email@example.com",
        entity=None)


@with_setup(setup, teardown)
def test_insert_user_failure_permissions():
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._insert_user(
            name=test_user,
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to insert a new user.")


@with_setup(setup, teardown)
def test_insert_user_failure_name_in_use():
    test_insert_user_success()
    with assert_raises(ClientErrorException) as cm:
        test_insert_user_success()
    assert_equals(cm.exception.msg, "User name is yet in use.")


@with_setup(setup, teardown)
def test_delete_user_success():
    test_insert_user_success()
    assert_is_not_none(admin._delete_user(name=test_user + "2"))


@with_setup(setup, teardown)
def test_delete_user_failure_permissions():
    test_insert_user_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._delete_user(name="non_existing_user")
    assert_equals(
        cm.exception.msg,
        "You are not permitted to delete this user.")


@with_setup(setup, teardown)
def test_delete_user_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._delete_user(name="non_existing_user")
    assert_equals(cm.exception.msg, "User does not exist.")


@with_setup(setup, teardown)
def test_update_user_success_status():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="INACTIVE",
            email="email@example.com",
            entity=None))
    admin._update_user(
        realm=None,
        name=test_user + "2",
        password=None,
        status="ACTIVE",
        email=None,
        entity=None)


@with_setup(setup, teardown)
def test_update_user_success_email():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None))
    admin._update_user(
        realm=None,
        name=test_user + "2",
        password=None,
        status=None,
        email="newemail@example.com",
        entity=None)


@with_setup(setup, teardown)
def test_update_user_success_entity():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None))
    admin._update_user(realm=None, name=test_user + "2", password=None,
                       status=None, email=None, entity="21")


@with_setup(setup, teardown)
def test_update_user_success_password():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None))
    admin._update_user(
        realm=None,
        name=test_user + "2",
        password="newsecret1P!",
        status=None,
        email=None,
        entity=None)


@with_setup(setup, teardown)
def test_update_user_failure_permissions_status():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="INACTIVE",
            email="email@example.com",
            entity=None))
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._update_user(
            realm=None,
            name=test_user + "2",
            password=None,
            status="ACTIVE",
            email=None,
            entity=None)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to update this user.")


@with_setup(setup, teardown)
def test_update_user_failure_permissions_email():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None))
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._update_user(
            realm=None,
            name=test_user + "2",
            password=None,
            status=None,
            email="newemail@example.com",
            entity=None)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to update this user.")


@with_setup(setup, teardown)
def test_update_user_failure_permissions_entity():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None))
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._update_user(
            realm=None,
            name=test_user + "2",
            password=None,
            status=None,
            email=None,
            entity=21)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to update this user.")


@with_setup(setup, teardown)
def test_update_user_failure_permissions_password():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None))
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._update_user(
            realm=None,
            name=test_user + "2",
            password="newsecret1P!",
            status=None,
            email=None,
            entity=None)
    assert_equals(
        cm.exception.msg,
        "You are not permitted to update this user.")


@with_setup(setup, teardown)
def test_update_user_failure_non_existing_user():
    with assert_raises(TransactionError) as cm:
        admin._update_user(
            realm=None,
            name="non-existing-user",
            password="newsecret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None)
    assert_equals(cm.exception.msg, "User does not exist.")


@with_setup(setup, teardown)
def test_update_user_failure_non_existing_entity():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="ACTIVE",
            email="email@example.com",
            entity=None))
    with assert_raises(ClientErrorException) as cm:
        admin._update_user(
            realm=None,
            name=test_user + "2",
            password=None,
            status=None,
            email=None,
            entity=100000)
    assert_equals(cm.exception.msg, "Entity does not exist.")


@with_setup(setup, teardown)
def test_retrieve_user_success():
    test_insert_user_success()
    assert_is_not_none(admin._retrieve_user(realm=None, name=test_user + "2"))


@with_setup(setup, teardown)
def test_retrieve_user_failure_permissions():
    test_insert_user_success()
    switch_to_normal_user()
    with assert_raises(AuthorizationException) as cm:
        admin._retrieve_user(realm=None, name=test_user + "2")
    assert_equals(
        cm.exception.msg,
        "You are not permitted to retrieve this user.")


@with_setup(setup, teardown)
def test_retrieve_user_failure_non_existing():
    with assert_raises(TransactionError) as cm:
        admin._retrieve_user(realm=None, name="non_existing")
    assert_equals(cm.exception.msg, "User does not exist.")


@with_setup(setup, teardown)
def test_login_with_inactive_user_failure():
    assert_is_not_none(
        admin._insert_user(
            name=test_user + "2",
            password="secret1P!",
            status="INACTIVE",
            email="email@example.com",
            entity=None))
    configure_connection(username=test_user + "2", password="secret1P!")
    with assert_raises(LoginFailedException):
        get_connection()._login()
