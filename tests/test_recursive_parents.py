# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 2016-01-14.
"""
from nose.tools import (ok_, eq_)
from caosdb.common.models import (Record, RecordType)


def test_recursive_parents():
    # inheritance structure:
    #    A
    #   / \
    #  B  B2
    #   \ /
    #    C
    #    |
    #    c
    A = RecordType(name="A")
    B = RecordType(name="B").add_parent(A)
    B2 = RecordType(name="B2").add_parent(A)
    C = RecordType(name="C").add_parent(B).add_parent(B2)
    c = Record(name="c").add_parent(C)

    parents = C.get_parents_recursively()
    eq_(len(parents), 3)
    ok_(A in parents)
    ok_(B in parents)
    ok_(B2 in parents)

    parents = c.get_parents_recursively()
    eq_(len(parents), 4)
    ok_(A in parents)
    ok_(B in parents)
    ok_(B2 in parents)
    ok_(C in parents)

    # Now do a time travel and great-grand-parentize yourself...
    A.add_parent(C)

    parents = C.get_parents_recursively()
    eq_(len(parents), 4)
    ok_(A in parents)
    ok_(B in parents)
    ok_(B2 in parents)
    ok_(C in parents)


def test_entity_has_parent():
    # inheritance structure:
    #    A
    #   / \
    #  B  B2
    #   \ /
    #    C
    #    |
    #    c
    A = RecordType(name="A")
    B = RecordType(name="B").add_parent(A)
    B2 = RecordType(name="B2", id=42).add_parent(A)
    C = RecordType(name="C").add_parent(B).add_parent(B2)
    c = Record(name="c").add_parent(C)

    ok_(C.has_parent(B))
    ok_(c.has_parent(B))
    ok_(c.has_parent(A))

    ok_(not C.has_parent(C))
    ok_(not A.has_parent(C))
    ok_(not B.has_parent(C))

    # Now do a time travel and great-grand-parentize yourself...
    A.add_parent(C)

    ok_(C.has_parent(C))

    # Non-recursive tests
    ok_(C.has_parent(B, recursive=False))
    ok_(not c.has_parent(B, recursive=False))
    ok_(not c.has_parent(A, recursive=False))
    ok_(not C.has_parent(C, recursive=False))

    # Works by name or ID
    fake_B_name = RecordType(name="B")
    fake_C_name = RecordType(name="not C")

    ok_(c.has_parent(fake_B_name, check_name=True))
    ok_(not c.has_parent(fake_C_name, check_name=True))

    fake_B_id = RecordType(id=42)
    fake_C_id = RecordType(id=23)

    ok_(c.has_parent(fake_B_id, check_name=False, check_id=True))
    ok_(not c.has_parent(fake_C_id, check_name=False, check_id=True))

    fake_B_name_id = RecordType(name="B", id=42)
    fake_C_name_id = RecordType(name="not C", id=23)

    ok_(c.has_parent(fake_B_name_id,
                     check_name=True, check_id=True))
    ok_(not c.has_parent(fake_C_name_id,
                         check_name=True, check_id=True))
