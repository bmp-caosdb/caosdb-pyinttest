# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import caosdb


def test_recordtypes():
    from nose.tools import assert_is_not_none  # @UnresolvedImport
    from sys import hexversion
    from sys import maxsize as maxint  # @Reimport
    from random import randint

    try:
        """recordtype without any property."""
        recty1 = caosdb.RecordType(
            name="PyTestRecordType" + hex(randint(0, maxint)),
            description="Desc PyTestRecordType")
        recty1.insert()
        assert_is_not_none(recty1.id)

        ''' and delete '''
        recty1.delete()

        ''' recordtype with a property '''
        ''' first inser property'''
        prop1 = caosdb.Property(
            name="PyTestProperty" + hex(
                randint(
                    0,
                    maxint)),
            description="Desc PyTestProperty",
            datatype="Double",
            unit="meter")
        prop1.insert()
        assert_is_not_none(prop1.id)

        ''' now insert recordtype'''
        recty2 = caosdb.RecordType(
            name="PyTestRecordType" + hex(randint(0, maxint)),
            description="Desc PyTestRecordType")
        recty2.add_property(prop1, importance="obligatory")
        recty2.insert()
        assert_is_not_none(recty2.id)

        ''' and delete '''
        recty2.delete()
        prop1.delete()

        ''' batch mode (without properties)'''
        c = caosdb.Container()
        recty3 = caosdb.RecordType(
            name="PyTestRecordType" + hex(randint(0, maxint)),
            description="Desc PyTestRecordType")
        recty4 = caosdb.RecordType(
            name="PyTestRecordType" + hex(randint(0, maxint)),
            description="Desc PyTestRecordType")
        c.extend([recty3, recty4])
        c.insert()

        c.delete()
    finally:
        try:
            recty3.delete()
        except BaseException:
            pass
        try:
            recty2.delete()
        except BaseException:
            pass
        try:
            recty1.delete()
        except BaseException:
            pass
        try:
            prop1.delete()
        except BaseException:
            pass


def test_record_types_as_reference_properties():
    from nose.tools import assert_is_not_none, assert_true, assert_equals  # @UnresolvedImport

    try:
        """recordtype without any property."""
        recty1 = caosdb.RecordType(
            name="TestRecordType1",
            description="Desc PyTestRecordType")
        ''' recordtype which references recty1'''
        recty2 = caosdb.RecordType(
            name="TestRecordType2",
            description="Desc PyTestRecordType")
        recty2.add_property(property=recty1, importance="Obligatory")

        c = caosdb.Container().extend([recty1, recty2]).insert()

        assert_true(recty1.is_valid())
        assert_is_not_none(recty1.id)
        assert_true(int(recty1.id) > 0)
        assert_true(recty2.is_valid())
        assert_is_not_none(recty2.id)
        assert_true(int(recty2.id) > 0)

        assert_is_not_none(recty2.get_properties().get_by_name(recty1.name))
        assert_is_not_none(
            recty2.get_properties().get_by_name(
                recty1.name).datatype)
        assert_equals(
            recty1.name.lower(), str(
                recty2.get_properties().get_by_name(
                    recty1.name).datatype).lower())

        c.delete()
    finally:
        try:
            recty2.delete()
        except BaseException:
            pass
        try:
            recty1.delete()
        except BaseException:
            pass
