# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on Jan 20, 2015.

@author: fitschen
"""
from nose.tools import assert_true, assert_equals, assert_is_not_none  # @UnresolvedImport


def test_leap_datamodel():
    import caosdb as h
    try:
        h.execute_query("FIND ENTITY WITH ID>100").delete()
    except BaseException:
        pass

    try:
        c = h.Container()

        '''<Property name="shortName" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='shortName', datatype='text'))

        '''<Property name="commonName" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='commonName', datatype='text'))

        '''<Property name="scientificName" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='scientificName', datatype='text'))

        '''<Property name="averageWeight" description="No Description" datatype="double" unit="g"/>'''
        c.append(h.Property(name='averageWeight', datatype='double', unit='g'))

        '''<Property name="weight" description="No Description" datatype="double" unit="g"/>'''
        c.append(h.Property(name='weight', datatype='double', unit='g'))

        '''<Property name="tattooID" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='tattooID', datatype='text'))

        '''<Property name="dateOfBirth" description="No Description" datatype="datetime"/>'''
        c.append(h.Property(name='dateOfBirth', datatype='datetime'))

        '''<Property name="animalRecord" description="No Description" datatype="file"/>'''
        c.append(h.Property(name='animalRecord', datatype='file'))

        '''<Property name="email" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='email', datatype='text'))

        '''<Property name="date" description="No Description" datatype="datetime"/>'''
        c.append(h.Property(name='date', datatype='datetime'))

        '''<Property name="experimenter" description="No Description" datatype="reference" reference="-1"/>'''
        c.append(h.Property(name='experimenter', datatype="Person"))
        ''' ??? '''

        '''<Property name="location" description="No Description" datatype="reference" reference="-2"/>'''
        c.append(h.Property(name='location', datatype='Location'))
        ''' ??? '''

        '''<Property name="labNotes" description="No Description" datatype="file"/>'''
        c.append(h.Property(name='labNotes', datatype='file'))

        '''<Property name="comment" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='comment', datatype='text'))

        '''<Property name="rating" description="No Description" datatype="integer"/>'''
        c.append(h.Property(name='rating', datatype='integer'))

        '''<Property name="pulseSettings" description="No Description" datatype="file"/>'''
        c.append(h.Property(name='pulseSettings', datatype='file'))

        '''<Property name="acqRawData" description="No Description" datatype="file"/>'''
        c.append(h.Property(name='acqRawData', datatype='file'))

        '''<Property name="country" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='country', datatype='text'))

        '''<Property name="locality" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='locality', datatype='text'))

        '''<Property name="building" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='building', datatype='text'))

        '''<Property name="room" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='room', datatype='text'))

        '''<Property name="length" description="No Description" datatype="double" unit="cm"/>'''
        c.append(h.Property(name='length', datatype='double', unit='cm'))

        '''<Property name="area" description="No Description" datatype="double" unit="cm^2"/>'''
        c.append(h.Property(name='area', datatype='double', unit='cm^2'))

        '''<Property name="distance" description="No Description" datatype="double" unit="cm"/>'''
        c.append(h.Property(name='distance', datatype='double', unit='cm'))

        '''<Property name="voltageScaling" description="No Description" datatype="double"/>'''
        c.append(h.Property(name='voltageScaling', datatype='double'))

        '''<Property name="currentScaling" description="No Description" datatype="double"/>'''
        c.append(h.Property(name='currentScaling', datatype='double'))

        '''<Property name="maxVoltage" description="No Description" datatype="double" unit="V"/>'''
        c.append(h.Property(name='maxVoltage', datatype='double', unit='V'))

        '''<Property name="maxCurrent" description="No Description" datatype="double" unit="A"/>'''
        c.append(h.Property(name='maxCurrent', datatype='double', unit='A'))

        '''<Property name="model" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='model', datatype='text'))

        '''<Property name="manufacturer" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='manufacturer', datatype='text'))

        '''<Property name="serialNumber" description="No Description" datatype="text"/>'''
        c.append(h.Property(name='serialNumber', datatype='text'))

        '''<Property name="maxFramerate" description="No Description" datatype="double" unit="Hz"/>'''
        c.append(h.Property(name='maxFramerate', datatype='double', unit='Hz'))

        '''<Property name="width" description="No Description" datatype="integer"/>'''
        c.append(h.Property(name='width', datatype='integer'))

        '''<Property name="height" description="No Description" datatype="integer"/>'''
        c.append(h.Property(name='height', datatype='integer'))

        '''<Property name="multirecXmlFile" description="No Description" datatype="file"/>'''
        c.append(h.Property(name='multirecXmlFile', datatype='file'))

        '''<Property name="multirecDataFile" description="No Description" datatype="file"/>'''
        c.append(h.Property(name='multirecDataFile', datatype='file'))

        '''<Property name="startTime" description="No Description" datatype="datetime"/>'''
        c.append(h.Property(name='startTime', datatype='datetime'))

        '''<Property name="endTime" description="No Description" datatype="datetime"/>'''
        c.append(h.Property(name='endTime', datatype='datetime'))

        '''<RecordType description="No Description" name="Sex">'''
        '''  <Property name="shortName" importance="obligatory"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='Sex')
                 .add_property(name='shortName', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="Species">'''
        '''  <Property name="commonName" importance="obligatory"/>'''
        '''  <Property name="scientificName" importance="obligatory"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='Species')
                 .add_property(name='commonName', importance='obligatory')
                 .add_property(name='scientificName', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="Race">'''
        '''  <Property name="averageWeight" importance="obligatory"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='Race')
                 .add_property(name='averageWeight', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="Animal">'''
        '''  <Property name="Species" importance="obligatory" datatype="reference"/>'''
        '''  <Property name="Race" importance="obligatory" datatype="reference"/>'''
        '''  <Property name="Sex" importance="obligatory" datatype="reference"/>'''
        '''  <Property name="tattooID" importance="obligatory"/>'''
        '''  <Property name="dateOfBirth" importance="obligatory"/>'''
        '''  <Property name="weight" importance="obligatory"/>'''
        '''  <Property name="animalRecord" importance="obligatory"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='Animal')
                 .add_property(name='Species', importance='obligatory')
                 .add_property(name='Race', importance='obligatory')
                 .add_property(name='Sex', importance='obligatory')
                 .add_property(name='tattooID', importance='obligatory')
                 .add_property(name='dateOfBirth', importance='obligatory')
                 .add_property(name='weight', importance='obligatory')
                 .add_property(name='animalRecord', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="Experiment">'''
        '''  <Property name="date" importance="obligatory"/>'''
        '''  <Property name="experimenter" importance="obligatory"/>'''
        '''  <Property name="location" importance="obligatory"/>'''
        '''  <Property name="labNotes" importance="obligatory"/>'''
        '''  <Property name="comment" importance="obligatory"/>'''
        '''  <Property name="rating" importance="recommended"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='Experiment')
                 .add_property(name='date', importance='obligatory')
                 .add_property(name='experimenter', importance='obligatory')
                 .add_property(name='location', importance='obligatory')
                 .add_property(name='labNotes', importance='obligatory')
                 .add_property(name='comment', importance='obligatory')
                 .add_property(name='rating', importance='recommended')
                 )

        '''<RecordType description="No Description" name="LEAP_Experiment">'''
        '''  <Property name="Animal" datatype="reference"/>'''
        '''  <Property name="ElectrodeSetup" datatype="reference"/>'''
        '''  <Property name="ShockSetup" datatype="reference"/>'''
        '''  <Property name="date" importance="obligatory"/>'''
        '''  <Property name="experimenter" importance="obligatory"/>'''
        '''  <Property name="location" importance="obligatory"/>'''
        '''  <Property name="labNotes" importance="obligatory"/>'''
        '''  <Property name="comment" importance="obligatory"/>'''
        '''  <Property name="rating" importance="recommended"/>'''
        '''  <Parent name="Experiment"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='LEAP_Experiment')
                 .add_parent(name='Experiment', inheritance='none')
                 .add_property(name='Animal', importance='obligatory')
                 .add_property(name='ElectrodeSetup', importance='obligatory')
                 .add_property(name='ShockSetup', importance='obligatory')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 .add_property(name='date', importance='obligatory')
                 .add_property(name='experimenter', importance='obligatory')
                 .add_property(name='location', importance='obligatory')
                 .add_property(name='labNotes', importance='obligatory')
                 .add_property(name='comment', importance='obligatory')
                 .add_property(name='rating', importance='recommended')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 )

        '''<RecordType description="No Description" name="LEAP_ExVivoExperiment">'''
        '''  <Property name="pulseSettings" importance="obligatory"/>'''
        '''  <Property name="acqRawData" importance="obligatory"/>'''
        '''  <Property name="Animal" datatype="reference"/>'''
        '''  <Property name="ElectrodeSetup" datatype="reference"/>'''
        '''  <Property name="ShockSetup" datatype="reference"/>'''
        '''  <Property name="date" importance="obligatory"/>'''
        '''  <Property name="experimenter" importance="obligatory"/>'''
        '''  <Property name="location" importance="obligatory"/>'''
        '''  <Property name="labNotes" importance="obligatory"/>'''
        '''  <Property name="comment" importance="obligatory"/>'''
        '''  <Property name="rating" importance="recommended"/>'''
        '''  <Parent name="LEAP_Experiment"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='LEAP_ExVivoExperiment')
                 .add_parent(name='LEAP_Experiment', inheritance='none')
                 .add_property(name='pulseSettings', importance='obligatory')
                 .add_property(name='acqRawData', importance='obligatory')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 .add_property(name='Animal', importance='obligatory')
                 .add_property(name='ElectrodeSetup', importance='obligatory')
                 .add_property(name='ShockSetup', importance='obligatory')
                 .add_property(name='date', importance='obligatory')
                 .add_property(name='experimenter', importance='obligatory')
                 .add_property(name='location', importance='obligatory')
                 .add_property(name='labNotes', importance='obligatory')
                 .add_property(name='comment', importance='obligatory')
                 .add_property(name='rating', importance='recommended')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 )

        '''<RecordType description="No Description" name="LEAP_InVivoExperiment">'''
        '''  <Property name="pulseSettings" importance="obligatory"/>'''
        '''  <Property name="acqRawData" importance="obligatory"/>'''
        '''  <Property name="MultiRecRecording" importance="obligatory" datatype="reference"/>'''
        '''  <Property name="Animal" datatype="reference"/>'''
        '''  <Property name="ElectrodeSetup" datatype="reference"/>'''
        '''  <Property name="ShockSetup" datatype="reference"/>'''
        '''  <Property name="date" importance="obligatory"/>'''
        '''  <Property name="experimenter" importance="obligatory"/>'''
        '''  <Property name="location" importance="obligatory"/>'''
        '''  <Property name="labNotes" importance="obligatory"/>'''
        '''  <Property name="comment" importance="obligatory"/>'''
        '''  <Property name="rating" importance="recommended"/>'''
        '''  <Parent name="LEAP_Experiment"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='LEAP_InVivoExperiment')
                 .add_parent(name='LEAP_Experiment', inheritance='none')
                 .add_property(name='pulseSettings', importance='obligatory')
                 .add_property(name='acqRawData', importance='obligatory')
                 .add_property(name='MultiRecRecording', importance='obligatory')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 .add_property(name='Animal', importance='obligatory')
                 .add_property(name='ElectrodeSetup', importance='obligatory')
                 .add_property(name='ShockSetup', importance='obligatory')
                 .add_property(name='date', importance='obligatory')
                 .add_property(name='experimenter', importance='obligatory')
                 .add_property(name='location', importance='obligatory')
                 .add_property(name='labNotes', importance='obligatory')
                 .add_property(name='comment', importance='obligatory')
                 .add_property(name='rating', importance='recommended')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 )

        '''<RecordType description="No Description" name="ExperimentGroup">'''
        '''  <Property name="Experiment" importance="obligatory" datatype="reference"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='ExperimentGroup')
                 .add_property(name='Experiment', importance='obligatory')
                 )

        '''<RecordType description="No Description" id="-1" name="Person">'''
        '''  <Property name="email" importance="obligatory"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='Person')
                 .add_property(name='email', importance='obligatory')
                 )

        '''<RecordType description="No Description" id="-2" name="Location">'''
        '''  <Property name="country" importance="obligatory"/>'''
        '''  <Property name="locality" importance="obligatory"/>'''
        '''  <Property name="building" importance="obligatory"/>'''
        '''  <Property name="room" importance="obligatory"/>'''
        '''</RecordType>'''
        c.append(h.RecordType(name='Location')
                 .add_property(name='country', importance='obligatory')
                 .add_property(name='locality', importance='obligatory')
                 .add_property(name='building', importance='obligatory')
                 .add_property(name='room', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="ElectrodeSetup">
             <Property name="length" importance="obligatory"/>
             <Property name="area" importance="obligatory"/>
             <Property name="distance" importance="obligatory"/>
           </RecordType>'''
        c.append(h.RecordType(name='ElectrodeSetup')
                 .add_property(name='length', importance='obligatory')
                 .add_property(name='area', importance='obligatory')
                 .add_property(name='distance', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="ShockSetup">
             <Property name="ShockDevice" importance="obligatory" datatype="reference"/>
             <Property name="voltageScaling" importance="obligatory"/>
             <Property name="currentScaling" importance="obligatory"/>
             <Property name="maxVoltage" importance="obligatory"/>
             <Property name="maxCurrent" importance="obligatory"/>
           </RecordType>'''
        c.append(h.RecordType(name='ShockSetup')
                 .add_property(name='ShockDevice', importance='obligatory')
                 .add_property(name='voltageScaling', importance='obligatory')
                 .add_property(name='currentScaling', importance='obligatory')
                 .add_property(name='maxVoltage', importance='obligatory')
                 .add_property(name='maxCurrent', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="ShockDevice">
             <Property name="maxVoltage" importance="obligatory"/>
             <Property name="maxCurrent" importance="obligatory"/>
             <Property name="model" importance="obligatory"/>
             <Property name="manufacturer" importance="obligatory"/>
             <Property name="serialNumber" importance="obligatory"/>
             <Parent name="Device"/>
           </RecordType>'''
        c.append(h.RecordType(name='ShockDevice')
                 .add_parent(name='Device', inheritance='none')
                 .add_property(name='maxVoltage', importance='obligatory')
                 .add_property(name='maxCurrent', importance='obligatory')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 .add_property(name='model', importance='obligatory')
                 .add_property(name='manufacturer', importance='obligatory')
                 .add_property(name='serialNumber', importance='obligatory')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 )

        '''<RecordType description="No Description" name="Device">
             <Property name="model" importance="obligatory"/>
             <Property name="manufacturer" importance="obligatory"/>
             <Property name="serialNumber" importance="obligatory"/>
           </RecordType>'''
        c.append(h.RecordType(name='Device')
                 .add_property(name='model', importance='obligatory')
                 .add_property(name='manufacturer', importance='obligatory')
                 .add_property(name='serialNumber', importance='obligatory')
                 )

        '''<RecordType description="No Description" name="Camera">
             <Property name="maxFramerate" importance="obligatory"/>
             <Property name="width" importance="obligatory"/>
             <Property name="height" importance="obligatory"/>
             <Property name="model" importance="obligatory"/>
             <Property name="manufacturer" importance="obligatory"/>
             <Property name="serialNumber" importance="obligatory"/>
             <Parent name="Device"/>
           </RecordType>'''
        c.append(h.RecordType(name='Camera')
                 .add_parent(name='Device', inheritance='none')
                 .add_property(name='maxFramerate', importance='obligatory')
                 .add_property(name='width', importance='obligatory')
                 .add_property(name='height', importance='obligatory')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 .add_property(name='model', importance='obligatory')
                 .add_property(name='manufacturer', importance='obligatory')
                 .add_property(name='serialNumber', importance='obligatory')
                 # should be added by server as soon as the inheritance flag is
                 # working
                 )

        '''<RecordType description="No Description" name="MultiRecRecording">
             <Property name="Camera" datatype="reference"/>
             <Property name="multirecXmlFile" importance="obligatory"/>
             <Property name="multirecDataFile" importance="obligatory"/>
             <Property name="startTime" importance="obligatory"/>
             <Property name="endTime" importance="obligatory"/>
           </RecordType>'''
        c.append(
            h.RecordType(
                name='MultiRecRecording') .add_property(
                name='Camera',
                importance='obligatory') .add_property(
                name='multirecXmlFile',
                importance='obligatory') .add_property(
                    name='multirecDataFile',
                    importance='obligatory') .add_property(
                        name='startTime',
                        importance='obligatory') .add_property(
                            name='endTime',
                importance='obligatory'))

        check_leap_container(c)

        c.insert()
        assert_true(c.is_valid())
        check_leap_container(c)

        c2 = h.Container()
        for e in c:
            c2.append(e.id)

        print(c2)
        c2.retrieve()
        print(c2)
        assert_true(c2.is_valid())
        check_leap_container(c2)

    finally:
        try:
            c.delete()
        except BaseException:
            pass


def check_leap_container(c):
    assert_equals(55, len(c))
    assert_is_not_none(c.get_entity_by_name("MultiRecRecording"))
    assert_equals(5, len(c.get_entity_by_name(
        "MultiRecRecording").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Camera"))
    assert_equals(6, len(c.get_entity_by_name("Camera").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Device"))
    assert_equals(3, len(c.get_entity_by_name("Device").get_properties()))

    assert_is_not_none(c.get_entity_by_name("ShockDevice"))
    assert_equals(5, len(c.get_entity_by_name("ShockDevice").get_properties()))

    assert_is_not_none(c.get_entity_by_name("ShockSetup"))
    assert_equals(5, len(c.get_entity_by_name("ShockSetup").get_properties()))

    assert_is_not_none(c.get_entity_by_name("ElectrodeSetup"))
    assert_equals(3, len(c.get_entity_by_name(
        "ElectrodeSetup").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Location"))
    assert_equals(4, len(c.get_entity_by_name("Location").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Person"))
    assert_equals(1, len(c.get_entity_by_name("Person").get_properties()))

    assert_is_not_none(c.get_entity_by_name("ExperimentGroup"))
    assert_equals(1, len(c.get_entity_by_name(
        "ExperimentGroup").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Experiment"))
    assert_equals(6, len(c.get_entity_by_name("Experiment").get_properties()))

    assert_is_not_none(c.get_entity_by_name("LEAP_Experiment"))
    assert_equals(9, len(c.get_entity_by_name(
        "LEAP_Experiment").get_properties()))

    assert_is_not_none(c.get_entity_by_name("LEAP_InVivoExperiment"))
    assert_equals(12, len(c.get_entity_by_name(
        "LEAP_InVivoExperiment").get_properties()))

    assert_is_not_none(c.get_entity_by_name("LEAP_ExVivoExperiment"))
    assert_equals(11, len(c.get_entity_by_name(
        "LEAP_ExVivoExperiment").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Animal"))
    assert_equals(7, len(c.get_entity_by_name("Animal").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Race"))
    assert_equals(1, len(c.get_entity_by_name("Race").get_properties()))

    assert_is_not_none(c.get_entity_by_name("Sex"))
    assert_equals(1, len(c.get_entity_by_name("Sex").get_properties()))
