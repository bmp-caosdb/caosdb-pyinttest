# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 21.03.2017.

@author: tf
"""

import caosdb as db
from nose.tools import nottest, assert_true, assert_raises, assert_equals, with_setup, assert_is_not_none  # @UnresolvedImport


def setup_module():
    pass


def teardown_module():
    teardown()


def setup():
    try:
        db.execute_query("FIND Test*").delete()
    except BaseException:
        pass


def teardown():
    try:
        db.execute_query("FIND Test*").delete()
    except BaseException:
        pass


@with_setup(setup, teardown)
def test_property_insertion():
    p = db.Property(
        name="TestShortName",
        datatype=db.TEXT,
        value="TestSN").add_parent(
        name="name").insert()
    assert_true(p.is_valid())

    return p


@with_setup(setup, teardown)
def test_property_query():
    pid = test_property_insertion().id
    assert_equals(db.execute_query("FIND TestShortName", unique=True).id, pid)

    # the short name does not apply to the property itself!!!
    assert_equals(len(db.execute_query("FIND TestSN")), 0)


@with_setup(setup, teardown)
def test_recordtype_insertion_separately_prop_by_id_direct_name_child():
    pid = test_property_insertion().id
    rt = db.RecordType(
        name="TestRecordType").add_property(
        id=pid, value="TestRT").insert()
    assert_true(rt.is_valid())

    return rt


@with_setup(setup, teardown)
def test_recordtype_is_stored_correctly():
    test_recordtype_insertion_separately_prop_by_id_direct_name_child()

    rt = db.RecordType(name="TestRecordType").retrieve()
    assert_true(rt.is_valid())
    assert_equals(len(rt.get_properties()), 1)
    assert_is_not_none(rt.get_property("TestShortName"))
    assert_equals(rt.get_property("TestShortName").value, "TestRT")


@with_setup(setup, teardown)
def test_recordtype_insertion_container_prop_by_tempid_direct_name_child():
    p = db.Property(
        name="TestShortName",
        datatype=db.TEXT,
        value="TestSN").add_parent(
        name="name")
    p.id = -1
    rt = db.RecordType(
        name="TestRecordType").add_property(
        id=-1, value="TestRT")
    c = db.Container().extend([p, rt])
    c.insert()
    assert_true(c.is_valid())
    assert_true(p.is_valid())
    assert_true(rt.is_valid())
    return rt


@with_setup(setup, teardown)
def test_recordtype_insertion_container_prop_by_name_direct_name_child():
    p = db.Property(
        name="TestShortName",
        datatype=db.TEXT,
        value="TestSN").add_parent(
        name="name")
    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestShortName",
        value="TestRT")
    c = db.Container().extend([p, rt])
    c.insert()
    assert_true(c.is_valid())
    assert_true(p.is_valid())
    assert_true(rt.is_valid())
    return rt


@with_setup(setup, teardown)
def test_recordtype_insertion_separately_prop_by_name_direct_name_child():
    test_property_insertion()
    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestShortName",
        value="TestRT").insert()
    assert_true(rt.is_valid())
    return rt


@with_setup(setup, teardown)
def test_recordtpye_insertion_with_indirect_child_with_existing_parent():
    test_property_insertion()
    p = db.Property(
        name="TestExtraShortName").add_parent(
        name="TestShortName").insert()
    assert_true(p.is_valid())

    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestExtraShortName",
        value="TestRT").insert()
    assert_true(rt.is_valid())
    return rt


@with_setup(setup, teardown)
def test_recordtpye_insertion_with_indirect_child_with_new_parent():
    parp = db.Property(name="TestShortName").add_parent(name="name")
    p = db.Property(name="TestExtraShortName").add_parent(name="TestShortName")
    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestExtraShortName",
        value="TestRT")
    c = db.Container().extend([parp, p, rt]).insert()

    assert_true(c.is_valid())
    assert_true(p.is_valid())
    assert_true(parp.is_valid())
    assert_true(rt.is_valid())

    return rt


@nottest
def do_unique_query_test(call, queries):
    setup()
    rtid = call().id
    for q in queries:
        assert_equals(db.execute_query(q, unique=True).id, rtid)
    teardown()


def test_recordtype_query():
    do_unique_query_test(
        test_recordtype_insertion_separately_prop_by_id_direct_name_child, [
            "FIND TestRecordType", "FIND TestRT"])
    do_unique_query_test(
        test_recordtype_insertion_separately_prop_by_name_direct_name_child, [
            "FIND TestRecordType", "FIND TestRT"])
    do_unique_query_test(
        test_recordtype_insertion_container_prop_by_name_direct_name_child, [
            "FIND TestRecordType", "FIND TestRT"])
    do_unique_query_test(
        test_recordtype_insertion_container_prop_by_tempid_direct_name_child, [
            "FIND TestRecordType", "FIND TestRT"])
    do_unique_query_test(
        test_recordtpye_insertion_with_indirect_child_with_new_parent, [
            "FIND TestRecordType", "FIND TestRT"])
    do_unique_query_test(
        test_recordtpye_insertion_with_indirect_child_with_existing_parent, [
            "FIND TestRecordType", "FIND TestRT"])


@with_setup(setup, teardown)
def test_query_with_pov():
    db.RecordType(name="TestExperiment").insert()
    db.Property(
        name="TestSynonym",
        datatype=db.TEXT).add_parent(
        name="name").insert()
    db.Property(
        name="TestRating",
        datatype=db.INTEGER).add_property(
        name="TestSynonym",
        value="TestScore").insert()
    rec = db.Record(
        name="TestRecord").add_parent(
        name="TestExperiment").add_property(
            name="TestRating",
        value=4).insert()
    assert_equals(
        db.execute_query(
            "FIND TestExperiment WHICH HAS A TestRating=4",
            unique=True).id,
        rec.id)
    assert_equals(
        db.execute_query(
            "FIND TestExperiment WHICH HAS A TestRating>3",
            unique=True).id,
        rec.id)
    assert_equals(
        db.execute_query(
            "FIND TestExperiment WHICH HAS A TestScore=4",
            unique=True).id,
        rec.id)
    assert_equals(
        db.execute_query(
            "FIND TestExperiment WHICH HAS A TestScore>3",
            unique=True).id,
        rec.id)


@with_setup(setup, teardown)
def test_query_with_reference():
    db.RecordType(name="TestExperiment").insert()
    db.RecordType(name="TestPerson").insert()
    db.Property(
        name="TestGivenName",
        datatype=db.TEXT).add_parent(
        name="name").insert()
    db.Property(name="TestConductor", datatype="TestPerson").insert()
    db.Record(
        name="TestJohnDoe").add_parent(
        name="TestPerson").add_property(
            name="TestGivenName",
        value="John").insert()
    rec = db.Record(
        name="TestRecord").add_parent(
        name="TestExperiment").add_property(
            name="TestConductor",
        value="TestJohnDoe").insert()
    assert_equals(
        db.execute_query(
            "FIND TestExperiment WHICH HAS A TestConductor=TestJohnDoe",
            unique=True).id,
        rec.id)
    assert_equals(
        db.execute_query(
            "FIND TestExperiment WHICH HAS A TestConductor=John",
            unique=True).id,
        rec.id)


@with_setup(setup, teardown)
def test_query_with_back_reference():
    db.RecordType(name="TestPerson").insert()
    db.Property(
        name="TestSynonym",
        datatype=db.TEXT).add_parent(
        name="name").insert()
    db.RecordType(
        name="TestMeasurement").add_property(
        name="TestSynonym",
        value="TestObservation").insert()
    db.Property(name="TestConductor", datatype="TestPerson").insert()
    rec = db.Record(name="TestJohnDoe").add_parent(name="TestPerson").insert()
    db.Record(
        name="TestRecord").add_parent(
        name="TestMeasurement").add_property(
            name="TestConductor",
        value="TestJohnDoe").insert()
    assert_equals(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestMeasurement",
            unique=True).id,
        rec.id)
    assert_equals(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestMeasurement AS A TestConductor",
            unique=True).id,
        rec.id)
    assert_equals(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestObservation",
            unique=True).id,
        rec.id)
    assert_equals(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestObservation AS A TestConductor",
            unique=True).id,
        rec.id)
