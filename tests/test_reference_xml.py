# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on Jan 20, 2015.

@author: fitschen
"""

import caosdb
from caosdb.connection.connection import get_connection


def test_records():

    sent_xml = '<Insert>'\
        '<Property id ="-1" name="Email" datatype="text"/>'\
        '<RecordType id="-2" name="Person">'\
        ' <Property id="-1" importance="obligatory"/>'\
        '</RecordType>'\
        '<RecordType name="Experiment">'\
        ' <Property id="-2" name="experimenter" importance="obligatory" datatype="reference" reference="-2"/>'\
        '</RecordType>'\
        '</Insert>'

    con = get_connection()

    response = con.insert(entity_uri_segment=["Entity"],
                          body=sent_xml)

    c = caosdb.Container._response_to_entities(response)

    c.delete()
