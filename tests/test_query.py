# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 26.01.2015.

@author: tf
"""


import caosdb as h

# @UnresolvedImport
from nose.tools import assert_true, assert_equals, assert_is_not_none, with_setup
from caosdb.connection.connection import get_connection
from lxml import etree  # @UnresolvedImport


def setup():
    try:
        h.execute_query("FIND Test*").delete()
    except Exception as e:
        print(e)


def teardown():
    try:
        h.execute_query("FIND Test*").delete()
    except Exception as e:
        print(e)
    try:
        import os
        os.remove("test.dat")
    except Exception as e:
        print(e)


@with_setup(setup, teardown)
def test_query_with_reference_by_parent():
    h.RecordType("TestExperiment").insert()
    h.RecordType("TestProtocolLog").insert()
    h.RecordType("TestSpecialProtocolLog").add_parent(
        name="TestProtocolLog").insert()

    sp_rec = h.Record("TestSpecialProtocolLog1").add_parent(
        name="TestSpecialProtocolLog").insert()
    exp_rec = h.Record("TestExperiment1").add_parent(
        name="TestExperiment").add_property(
        name="TestProtocolLog",
        datatype=h.LIST("TestProtocolLog"),
        value=[
            sp_rec.id]).insert()
    assert_equals(2, len(h.execute_query("FIND TestExperiment")))
    assert_equals(
        exp_rec.id,
        h.execute_query(
            "FIND TestExperiment WHICH HAS A TestProtocolLog",
            unique=True).id)
    assert_equals(
        exp_rec.id,
        h.execute_query(
            "FIND TestExperiment.TestProtocolLog",
            unique=True).id)
    assert_equals(
        exp_rec.id, h.execute_query(
            "FIND TestExperiment WHICH REFERENCES " + str(sp_rec.id),
            unique=True).id)
    assert_equals(
        exp_rec.id,
        h.execute_query(
            "FIND TestExperiment WHICH REFERENCES TestSpecialProtocolLog1",
            unique=True).id)
    assert_equals(
        exp_rec.id,
        h.execute_query(
            "FIND TestExperiment WHICH REFERENCES TestSpecialProtocolLog",
            unique=True).id)
    assert_equals(exp_rec.id, h.execute_query(
        "FIND TestExperiment .-> " + str(sp_rec.id), unique=True).id)
    assert_equals(
        exp_rec.id,
        h.execute_query(
            "FIND TestExperiment .-> TestSpecialProtocolLog1",
            unique=True).id)
    assert_equals(
        exp_rec.id,
        h.execute_query(
            "FIND TestExperiment .-> TestSpecialProtocolLog",
            unique=True).id)

    assert_equals(0, len(h.execute_query(
        "FIND TestExperiment WHICH HAS A TestProtocolLog1")))
    assert_equals(0, len(h.execute_query(
        "FIND TestExperiment.TestProtocolLog1")))


@with_setup(setup, teardown)
def test_query_with_domains():
    person = h.RecordType("TestPerson").insert()
    h.Property("TestFirstName", datatype=h.TEXT).insert()
    h.Property("TestConductor", datatype=h.REFERENCE).insert()

    # TODO: new test for TestFirstName with overridden datatype=person
    dan = h.Record(
        name="TestDaniel").add_property(
        name="TestFirstName",
        value="Daniel").add_parent(person).insert()
    exp = h.RecordType(
        name="TestExperiment").add_property(
        name="TestConductor",
        datatype=person,
        value=dan.id).insert()

    assert_equals(
        h.execute_query(
            "FIND TestExperiment WHICH HAS A TestConductor->TestPerson",
            unique=True).id,
        exp.id)
    assert_equals(
        h.execute_query(
            "FIND TestExperiment WHICH HAS A TestConductor=TestPerson",
            unique=True).id,
        exp.id)
    assert_equals(
        h.execute_query(
            "FIND TestExperiment WHICH HAS A TestConductor=" + str(dan.id),
            unique=True).id, exp.id)
    assert_equals(
        h.execute_query(
            "FIND TestExperiment",
            unique=True).id,
        exp.id)
    assert_equals(
        h.execute_query(
            "FIND TestExperiment WHICH HAS A TestConductor WHICH has a TestFirstName=Daniel",
            unique=True).id,
        exp.id)


@with_setup(setup, teardown)
def test_query1():
    p = (
        h.Property(
            name="TestTextProperty",
            description="Test text property (from test_tenpoints.py)",
            datatype='text'))
    p.insert()

    assert_true(int(p.id) >= 100)
    assert_equals(p.id, h.execute_query("FIND TestTextProperty")[0].id)
    assert_equals(
        p.id,
        h.execute_query(
            "FIND TestTextProperty",
            unique=True).id)


@with_setup(setup, teardown)
def test_query2():
    # create testfile
    f = open("test.dat", "w")
    f.write("hello world\n")
    f.close()

    ''' prepare file record '''
    f = h.File(name="TestFile", path='testfiles/testfile', file="test.dat")
    f.insert()

    f2 = h.File(name="TestFile2", path='testfiles/testfile2', file="test.dat")
    f2.insert()

    assert_true(int(f.id) >= 100)
    assert_equals(2, len(h.execute_query("FIND FILE")))
    assert_equals(
        f.id,
        h.execute_query(
            "FIND FILE WHICH IS STORED AT testfiles/testfile",
            unique=True).id)
    assert_equals(f.id, h.File(path='testfiles/testfile').retrieve().id)


def test_query3():
    body = get_connection().retrieve(
        entity_uri_segments=["Entity"], query_dict={
            "query": None}, reconnect=True).read()
    print(body)
    xml = etree.fromstring(body)
    assert_equals(3, len(xml))
    assert_equals("query", xml[1].tag.lower())
    assert_equals("transactionbenchmark", xml[2].tag.lower())


@with_setup(setup, teardown)
def test_conjunction():
    rt = h.RecordType(name="TestConjunctionTest").insert()
    assert_true(rt.is_valid())

    pa = h.Property(
        name="TestConjunctionTestPropertyA",
        datatype="INTEGER").insert()
    assert_true(pa.is_valid())

    pb = h.Property(
        name="TestConjunctionTestPropertyB",
        datatype="INTEGER").insert()
    assert_true(pb.is_valid())

    ra = h.Record(
        name="TestA").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="0")
    rb = h.Record(
        name="TestB").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="1")
    rab = h.Record(
        name="TestAB").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="1")
    rn = h.Record(
        name="TestN").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="0")
    c = h.Container().extend([ra, rb, rab, rn]).insert()
    assert_true(c.is_valid())

    assert_equals(5, len(h.execute_query("FIND TestConjunctionTest")))

    assert_equals(4, len(h.execute_query("FIND Record TestConjunctionTest")))

    assert_equals(4, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA")))
    assert_equals(4, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyB")))
    assert_equals(2, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyB=1")))
    assert_equals(2, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyB=0")))
    assert_equals(2, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=1")))
    assert_equals(2, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=0")))

    assert_equals(1, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=1")))
    assert_equals(1, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=0")))
    assert_equals(1, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=1")))
    assert_equals(1, len(h.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=0")))

    assert_equals(4, len(h.execute_query(
        "FIND Record . TestConjunctionTestPropertyA")))
    assert_equals(4, len(h.execute_query(
        "FIND Record . TestConjunctionTestPropertyB")))
    assert_equals(2, len(h.execute_query(
        "FIND Record . TestConjunctionTestPropertyB=1")))
    assert_equals(2, len(h.execute_query(
        "FIND Record . TestConjunctionTestPropertyB=0")))
    assert_equals(2, len(h.execute_query(
        "FIND Record . TestConjunctionTestPropertyA=1")))
    assert_equals(2, len(h.execute_query(
        "FIND Record . TestConjunctionTestPropertyA=0")))

    assert_equals(1, len(h.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=1")))
    assert_equals(1, len(h.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=0")))
    assert_equals(1, len(h.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=1")))
    assert_equals(1, len(h.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=0")))


@with_setup(setup, teardown)
def test_disjunction():
    rt = h.RecordType(name="TestDisjunctionTest").insert()
    assert_true(rt.is_valid())

    pa = h.Property(
        name="TestDisjunctionTestPropertyA",
        datatype="INTEGER").insert()
    assert_true(pa.is_valid())

    pb = h.Property(
        name="TestDisjunctionTestPropertyB",
        datatype="DOUBLE").insert()
    assert_true(pb.is_valid())

    ra = h.Record(
        name="TestA").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="0")
    rb = h.Record(
        name="TestB").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="1")
    rab = h.Record(
        name="TestAB").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="1")
    rn = h.Record(
        name="TestN").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="0")
    c = h.Container().extend([ra, rb, rab, rn]).insert()
    assert_true(c.is_valid())

    assert_equals(5, len(h.execute_query("FIND TestDisjunctionTest")))

    assert_equals(4, len(h.execute_query("FIND Record TestDisjunctionTest")))

    assert_equals(4, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA")))
    assert_equals(4, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyB")))

    assert_equals(2, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyB=1")))
    assert_equals(2, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyB=0")))
    assert_equals(2, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1")))
    assert_equals(2, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0")))

    assert_equals(3, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR TestDisjunctionTestPropertyB=1")))
    assert_equals(3, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0 OR TestDisjunctionTestPropertyB=0")))
    assert_equals(3, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0 OR TestDisjunctionTestPropertyB=1")))
    assert_equals(3, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR TestDisjunctionTestPropertyB=0")))

    assert_equals(2, len(h.execute_query(
        "FIND TestDisjunctionTest . THE GREATEST TestDisjunctionTestPropertyB")))
    assert_equals(3, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR THE GREATEST TestDisjunctionTestPropertyB")))

    assert_equals(
        rn.id,
        h.execute_query(
            "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA='-1' OR ( TestDisjunctionTestPropertyB=0 AND TestDisjunctionTestPropertyA=0)",
            unique=True).id)
    assert_equals(3, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0 OR ( TestDisjunctionTestPropertyB=0 AND TestDisjunctionTestPropertyA=1)")))
    assert_equals(2, len(h.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR ( TestDisjunctionTestPropertyB=0 AND TestDisjunctionTestPropertyA=1)")))


@with_setup(setup, teardown)
def test_greatest():
    pAB = h.Property(name="TestPropertyAB", datatype=h.DOUBLE).insert()
    assert_true(pAB.is_valid())
    pA = h.Property(
        name="TestPropertyA",
        datatype=h.DOUBLE).add_parent(pAB).insert()
    assert_true(pA.is_valid())
    pB = h.Property(name="TestPropertyB", datatype=h.DOUBLE).add_parent(pAB)
    assert_equals(len(pB.get_parents()), 1)
    print(pB)
    print(pB)
    pB.insert()
    assert_true(pB.is_valid())

    rt = h.RecordType(name="TestRecordType").insert()
    assert_true(rt.is_valid())

    rec1 = h.Record(name="TestRecord1").add_parent(
        rt).add_property(pA, value=1.0).insert()
    assert_true(rec1.is_valid())

    c = h.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec1.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec1.id)

    c = h.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec1.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec1.id)

    rec2 = h.Record(name="TestRecord2").add_parent(
        rt).add_property(pA, value=2.0).insert()
    assert_true(rec2.is_valid())

    c = h.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec2.id)

    c = h.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec1.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec2.id)

    c = h.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec2.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec2.id)

    rec3 = h.Record(name="TestRecord3").add_parent(
        rt).add_property(pA, value=1.5).insert()
    assert_true(rec3.is_valid())

    c = h.execute_query(
        "FIND TestRecord3 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec3.id)

    c = h.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec2.id)

    c = h.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec1.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec2.id)

    c = h.execute_query(
        "FIND TestRecord3 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec3.id)

    c = h.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec2.id)

    c = h.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec1.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec2.id)

    rec4 = h.Record(name="TestRecord4").add_parent(
        rt).add_property(pB, value=0.1).insert()
    assert_true(rec4.is_valid())

    c = h.execute_query(
        "FIND TestRecord4 WHICH HAS THE GREATEST TestPropertyA")
    assert_equals(len(c), 0)

    c = h.execute_query(
        "FIND TestRecord4 WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert_equals(c.id, rec4.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert_equals(c.id, rec4.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec2.id)

    rec5 = h.Record(
        name="TestRecord5").add_parent(rt).add_property(
        pB,
        value=200).add_property(
            pA,
        value=1.5).insert()
    assert_true(rec5.is_valid())

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec5.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert_equals(c.id, rec5.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert_equals(c.id, rec2.id)

    rec6 = h.Record(
        name="TestRecord6").add_parent(rt).add_property(
        pAB,
        value=400).add_property(
            pB,
            value=100).add_property(
                pA,
        value=2).insert()
    assert_true(rec6.is_valid())

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert_equals(c.id, rec6.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert_equals(c.id, rec5.id)

    c = h.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA")
    assert_equals(c[0].id, rec2.id)
    assert_equals(c[1].id, rec6.id)


@with_setup(setup, teardown)
def test_wildcard_values():
    ptext = h.Property(name="TestTextProperty", datatype=h.TEXT).insert()
    rt = h.RecordType(
        name="TestRecordType").add_property(
        ptext, value="abcdefg").insert()

    assert_true(rt.is_valid())
    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE "abc*"',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc*',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc*efg',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc*g',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE a*',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *abc*',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *fg',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE a*f*g',
        unique=True)
    assert_equals(rt2.id, rt.id)

    rt2 = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE a*d*g',
        unique=True)
    assert_equals(rt2.id, rt.id)

    c = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc')
    assert_equals(0, len(c))

    c = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty = abc*')
    assert_equals(0, len(c))

    c = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty = *abc*')
    assert_equals(0, len(c))

    c = h.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *f')
    assert_equals(0, len(c))


def test_stored_at_wildcards():

    upload_file = open("test.dat", "w")
    upload_file.write("hello world\n")
    upload_file.close()

    global i
    i = 1

    def store_file(path):
        global i
        file_ = h.File(name="TestTestFile" + str(i),
                       description="Testfile Desc",
                       path=path,
                       file="test.dat")
        i += 1
        file_.insert()
        return file_

    file1 = store_file("test1.dat")

    file2 = store_file("/rootdir/test2.dat")

    file3 = store_file("/rootdir/subdir1/test3.dat")

    file4 = store_file("/rootdir/subdir1/subdir2/test4.dat")

    file5 = store_file("test5.dat")

    file6 = store_file("rootdir/test*6.dat")

    file7 = store_file("rootdir/subdir1/test%7.dat")

    file8 = store_file("rootdir/subdir1/test%8.dat")

    c = h.execute_query("FIND FILE WHICH IS STORED AT /**/subdir*/*.dat")
    assert_equals(len(c), 4)
    assert_is_not_none(c.get_entity_by_id(file3.id))
    assert_is_not_none(c.get_entity_by_id(file4.id))
    assert_is_not_none(c.get_entity_by_id(file7.id))
    assert_is_not_none(c.get_entity_by_id(file8.id))

    # Currently, this is implemented differently. See issue: #27 in
    # caosdb-server
    #c = h.execute_query("FIND FILE WHICH IS STORED AT /*.dat")
    #assert_equals(len(c), 2)
    # assert_is_not_none(c.get_entity_by_id(file1.id))
    # assert_is_not_none(c.get_entity_by_id(file5.id))

    c = h.execute_query("FIND FILE WHICH IS STORED AT *subdir**.dat")
    assert_equals(len(c), 4)
    assert_is_not_none(c.get_entity_by_id(file3.id))
    assert_is_not_none(c.get_entity_by_id(file4.id))
    assert_is_not_none(c.get_entity_by_id(file7.id))
    assert_is_not_none(c.get_entity_by_id(file8.id))

    c = h.execute_query("FIND FILE WHICH IS STORED AT *subdir1**.dat")
    assert_equals(len(c), 4)
    assert_is_not_none(c.get_entity_by_id(file3.id))
    assert_is_not_none(c.get_entity_by_id(file4.id))
    assert_is_not_none(c.get_entity_by_id(file7.id))
    assert_is_not_none(c.get_entity_by_id(file8.id))

    c = h.execute_query("FIND FILE WHICH IS STORED AT *subdir2**.dat")
    assert_equals(len(c), 1)
    assert_is_not_none(c.get_entity_by_id(file4.id))

    c = h.execute_query("FIND FILE WHICH IS STORED AT /rootdir/*.dat")
    assert_equals(len(c), 2)
    assert_is_not_none(c.get_entity_by_id(file2.id))
    assert_is_not_none(c.get_entity_by_id(file6.id))

    f = h.execute_query("FIND FILE WHICH IS STORED AT test1.dat", unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query("FIND FILE WHICH IS STORED AT /test1.dat", unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query("FIND FILE WHICH IS STORED AT *test1.dat", unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT /*test1.dat",
        unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query("FIND FILE WHICH IS STORED AT *1.dat", unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query("FIND FILE WHICH IS STORED AT /*1.dat", unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT **test1.dat",
        unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT /**test1.dat",
        unique=True)
    assert_equals(f.id, file1.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT rootdir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT /rootdir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT *rootdir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT /*rootdir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT **rootdir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT /**rootdir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT *dir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT /*dir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT **dir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    f = h.execute_query(
        "FIND FILE WHICH IS STORED AT /**dir/test2.dat",
        unique=True)
    assert_equals(f.id, file2.id)

    c = h.execute_query("FIND FILE WHICH IS STORED AT *.dat")
    assert_equals(len(c), 8)

    c = h.execute_query("FIND FILE WHICH IS STORED AT *test*.dat")
    assert_equals(len(c), 8)

    c = h.execute_query("FIND FILE WHICH IS STORED AT *test%*.dat")
    assert_equals(len(c), 2)
    assert_is_not_none(c.get_entity_by_id(file7.id))
    assert_is_not_none(c.get_entity_by_id(file8.id))

    f = h.execute_query(
        r"FIND FILE WHICH IS STORED AT *test\**.dat",
        unique=True)
    assert_equals(f.id, file6.id)


@with_setup(setup, teardown)
def test_int():
    pint = h.Property(name="TestIntegerProperty", datatype=h.INTEGER).insert()
    pdouble = h.Property(name="TestDoubleProperty", datatype=h.DOUBLE).insert()
    h.RecordType(
        name="TestRecordType").add_property(
        pint,
        value="100").add_property(
            pdouble,
        value="100.5").insert()

    h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>50",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>=50",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>=100",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<=100",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<=200",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<200",
        unique=True)

    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>200")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>=200")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty=200")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<50")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<=50")))

    h.execute_query("FIND TestRecordType WITH TestDoubleProperty", unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>50",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>=50",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>=100.5",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>=100.5",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<=100.5",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<=200",
        unique=True)
    h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<200",
        unique=True)

    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>200")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>=200")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty=200")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<50")))
    assert_equals(0, len(h.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<=50")))


@with_setup(setup, teardown)
def test_query_benchmark():
    h.Property("TestProperty", datatype=h.TEXT).insert()

    body = get_connection().retrieve(
        entity_uri_segments=["Entity"],
        query_dict={
            "query": "FIND TestProperty"},
        reconnect=True).read()
    print(body)
    xml = etree.fromstring(body)
    # see #4
    #assert_equals(3, len(xml))
    #assert_equals("query", xml[0].tag.lower())
    #assert_equals("transactionbenchmark", xml[0][3].tag.lower())
    #benchmark = xml[0][3]
    #assert_true(len(benchmark) > 0)
