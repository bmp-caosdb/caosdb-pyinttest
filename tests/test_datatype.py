# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 22.01.2017.

@author: tf
"""
import caosdb as db
# @UnresolvedImport
from nose.tools import nottest, assert_raises, with_setup, assert_true, assert_equals


def setup():
    try:
        db.execute_query("FIND Test*").delete()
    except BaseException:
        pass


def teardown():
    try:
        db.execute_query("FIND Test*").delete()
    except BaseException:
        pass


@with_setup(setup, teardown)
def test_override_with_non_existing_ref():
    rt1 = db.RecordType("TestRecordType1").insert()
    rt2 = db.RecordType("TestRecordType2").insert()
    db.Property("TestProperty", datatype=db.TEXT).insert()

    with assert_raises(db.TransactionError) as cm:
        db.Record(
            name="TestRecord").add_property(
            name="TestProperty",
            datatype=rt2,
            value="Non-Existing").add_parent(rt1).insert()
    assert_equals(
        cm.exception.get_errors()[0].msg,
        "Referenced entity does not exist.")


@with_setup(setup, teardown)
def test_override_with_existing_ref():
    rt1 = db.RecordType("TestRecordType1").insert()
    rt2 = db.RecordType("TestRecordType2").insert()
    db.Property("TestProperty", datatype=db.TEXT).insert()
    db.Record("TestRecord").add_parent(name="TestRecordType2").insert()

    rec2 = db.Record(
        name="TestRecord2").add_property(
        name="TestProperty",
        datatype=rt2,
        value="TestRecord").add_parent(rt1).insert()

    assert_true(rec2.is_valid())


@with_setup(setup, teardown)
def test_reference_datatype_sequencial():
    rt = db.RecordType(name="TestRT", description="TestRTDesc").insert()
    p = db.Property(
        name="TestProp",
        description="RefOnTestRT",
        datatype="TestRT").insert()

    assert_true(p.is_valid())

    dt = db.execute_query("FIND TestProp", unique=True).datatype
    assert_equals(dt, "TestRT")

    rt2 = db.RecordType(
        "TestRT2",
        description="TestRT2Desc").add_property(
        name="TestProp",
        value="TestRT").insert()
    assert_true(rt2.is_valid())
    assert_equals(rt2.get_property("TestProp").value, rt.id)
    assert_equals(rt2.get_property("TestProp").datatype, "TestRT")


@with_setup(setup, teardown)
def test_reference_datatype_at_once():
    rt = db.RecordType(name="TestRT")
    rt2 = db.RecordType(name="TestRT2").add_property(name="TestProp")
    rec = db.Record().add_parent(name="TestRT")
    rec2 = db.Record().add_parent(
        name="TestRT2").add_property(
        name="TestProp",
        value=rec)
    p = db.Property(name="TestProp", datatype="TestRT")

    c = db.Container().extend([rt, rt2, rec, rec2, p]).insert()
    assert_true(c.is_valid())


@with_setup(setup, teardown)
def test_generic_reference_success():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").insert()
    p = db.Property(name="TestP1", datatype=db.REFERENCE).insert()
    rec1 = db.Record(name="TestRec1").add_parent(name="TestRT1").insert()
    rec2 = db.Record(
        name="TestRec2").add_parent(
        name="TestRT2").add_property(
            name="TestP1",
        value=rec1.id).insert()

    assert_true(rt1.is_valid())
    assert_true(rt2.is_valid())
    assert_true(p.is_valid())
    assert_true(rec1.is_valid())
    assert_true(rec2.is_valid())


@with_setup(setup, teardown)
def test_generic_reference_failure():
    db.RecordType(name="TestRT2").insert()
    db.Property(name="TestP1", datatype=db.REFERENCE).insert()
    rec2 = db.Record(
        name="TestRec2").add_parent(
        name="TestRT2").add_property(
            name="TestP1",
        value="asdf")
    assert_raises(db.TransactionError, rec2.insert)


@with_setup(setup, teardown)
def test_unknown_datatype1():
    p = db.Property(name="TestP", datatype="Non-Existing")
    try:
        p.insert()
        assert_true(False)
    except db.TransactionError as e:
        assert_equals(e.msg, "Unknown datatype.")


@with_setup(setup, teardown)
def test_unknown_datatype2():
    p = db.Property(name="TestP", datatype="12345687654334567")
    try:
        p.insert()
        assert_true(False)
    except db.TransactionError as e:
        assert_equals(e.msg, "Unknown datatype.")


@with_setup(setup, teardown)
def test_unknown_datatype3():
    p = db.Property(name="TestP", datatype="-134")
    try:
        p.insert()
        assert_true(False)
    except db.TransactionError as e:
        assert_equals(e.msg, "Unknown datatype.")


@with_setup(setup, teardown)
def test_wrong_refid():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").insert()
    rt3 = db.RecordType(name="TestRT3").insert()
    p = db.Property(name="TestP1", datatype=rt1.id).insert()
    assert_true(p.is_valid())
    assert_true(rt1.is_valid())
    assert_true(rt2.is_valid())
    assert_true(rt3.is_valid())

    rec1 = db.Record().add_parent(name="TestRT1").insert()
    rec2 = db.Record().add_parent(name="TestRT2").insert()
    rec3 = db.Record().add_parent(
        name="TestRT3").add_property(
        name="TestP1",
        value=rec2.id)
    try:
        rec3.insert()
        assert_true(False)
    except db.TransactionError:
        assert_equals(
            rec3.get_property("TestP1").get_errors()[0].description,
            'Reference not qualified. The value of this Reference Property is to be a child of its data type.')

    rec4 = db.Record().add_parent(
        name="TestRT3").add_property(
        name="TestP1",
        value=rec1.id).insert()
    assert_true(rec4.is_valid())
