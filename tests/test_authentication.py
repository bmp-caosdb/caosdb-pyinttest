# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 20.01.2015.

@author: tf
"""

from caosdb.exceptions import LoginFailedException
import caosdb as h
from nose.tools import assert_false, assert_true, assert_is_none, assert_raises, assert_equals, assert_is_not_none, nottest  # @UnresolvedImport
from caosdb.connection.connection import _Connection


def test_https_support():
    from sys import hexversion
    if hexversion < 0x02070900:
        raise Exception("version " + str(hex(hexversion)))
    elif 0x02999999 < hexversion < 0x03020000:
        raise Exception("version " + str(hex(hexversion)))
    try:
        # python2
        from httplib import HTTPSConnection
        from urlparse import urlparse
    except:
        # python 3
        from urllib.parse import urlparse
        from http.client import HTTPSConnection
    import ssl

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # @UndefinedVariable
    context.verify_mode = ssl.CERT_REQUIRED
    if hasattr(context, "check_hostname"):
        context.check_hostname = True
    context.load_verify_locations(h.get_config().get("Connection", "cacert"))

    url = h.get_config().get("Connection", "url")
    fullurl = urlparse(url)

    http_con = HTTPSConnection(
        str(fullurl.netloc), timeout=200, context=context)

    http_con.request(method="GET", headers={}, url=str(fullurl.path) + "Info")
    r = http_con.getresponse()
    print(r.read())


def test_login_via_post_form_data_failure():
    with assert_raises(LoginFailedException) as cm:
        h.get_connection().post_form_data(
            "login", {
                "username": h.get_config().get("Connection", "username"),
                "password": "wrongpassphrase"
            })
