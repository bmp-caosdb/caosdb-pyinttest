# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 20.09.2013.

@author: Timm Fitschen
"""


def test_messages_dict_behavior():

    from caosdb.common.models import Message
    from caosdb.common.models import _Messages

    msgs = _Messages()

    # create Message
    msg = Message(
        type="HelloWorld",
        code=1,
        description="Greeting the world",
        body="Hello, world!")

    # append it to the _Messages
    msgs.append(msg)

    # use _Messages as list of Message objects
    for m in msgs:
        assert isinstance(m, Message)

    # remove it
    msgs.remove(msg)

    # ok append it again ...
    msgs.append(msg)
    # get it back via get(...) and the key tuple (type, code)
    assert id(msgs.get("HelloWorld", 1)) == id(msg)

    # delete Message via remove and the (type,code) tuple
    msgs.remove("HelloWorld", 1)
    assert msgs.get("HelloWorld", 1) is None

    # short version of adding/setting/resetting a new Message
    msgs["HelloWorld", 2] = "Greeting the world in German", "Hallo, Welt!"
    assert msgs["HelloWorld", 2] == (
        "Greeting the world in German", "Hallo, Welt!")
    msgs["HelloWorld", 2] = "Greeting the world in German", "Huhu, Welt!"
    assert msgs["HelloWorld", 2] == (
        "Greeting the world in German", "Huhu, Welt!")
    del msgs["HelloWorld", 2]
    assert msgs.get("HelloWorld", 2) is None

    # this Message has no code and no description (make easy things easy...)
    msgs["HelloWorld"] = "Hello!"
    assert msgs["HelloWorld"] == "Hello!"
