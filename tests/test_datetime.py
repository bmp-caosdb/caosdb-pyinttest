# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 27.10.2015.

@author: tf
"""

from nose.tools import assert_true, assert_false  # @UnresolvedImport
import caosdb as h

''' some helpful functions '''
old_stored = None


def update_rec(rec, datetime):
    global old_stored
    if old_stored == datetime:
        return
    rec.get_property("SimpleDateProperty").value = datetime
    rec.update()
    old_stored = datetime


def store(rec, datetime):
    update_rec(rec, datetime)
    assert_true(rec.is_valid())
    return h.Record(id=rec.id).retrieve().get_property(
        "SimpleDateProperty").value


def set_up_rec():
    global old_stored
    old_stored = None
    p = h.Property(name="SimpleDateProperty", datatype="DATETIME").insert()
    assert_true(p.is_valid())

    rt = h.RecordType(
        name="SimpleRecordType").add_property(
        p, importance="OBLIGATORY").insert()
    assert_true(rt.is_valid())

    rec = h.Record(name="SimpleRecord").add_parent(rt).add_property(p).insert()
    assert_true(rec.is_valid())

    return rec


def query_assertion(rec, stored, op, queried):
    update_rec(rec, stored)
    assert_true(rec.is_valid())

    q = h.execute_query(
        "FIND RECORD SimpleRecord WHICH HAS A SimpleDateProperty" +
        op +
        queried)
    return len(q) == 1


''' the actual tests '''


def test_utcdatetime_storage():
    try:
        rec = set_up_rec()

        utcs = [
            "2015-12-24T20:15:30",
            "2015-12-24T20:15:30.123",
            "-9999-01-01T00:00:00",
            "-9999-01-01T00:00:00.0",
            "9999-12-31T23:59:59",
            "9999-12-31T23:59:59.999999999",
            "0001-01-01T00:00:00",
            "0001-01-01T00:00:00.0",
            "-0001-12-31T23:59:59",
            "-0001-12-31T23:59:59.999999"]
        for d in utcs:
            assert_true(d in store(rec, d))

    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass


def test_date_storage():
    try:
        rec = set_up_rec()

        dates = [
            "2015-12-24",
            "2015",
            "2015-12",
            "9999-12-31",
            "9999-12",
            "9999",
            "-9999-01-01",
            "-9999-01",
            "-9999",
            "0001-01-01",
            "0001-01",
            "0001",
            "-0001-12-31",
            "-0001-12",
            "-0001"]
        for d in dates:
            print(d)
            assert_true(d in store(rec, d))

    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass


def test_in_operator():
    try:
        rec = set_up_rec()

        # Date YYYY-MM-ddThh:mm:ss
        d = "2015-05-05T20:15:30"
        assert_true(query_assertion(rec, d, " IN ", d))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05T20:15"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05T20"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05"))
        assert_true(query_assertion(rec, d, " IN ", "2015"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T20:15:30.124"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T20:15:31.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T20:15:31"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T20:16:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T20:16:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T20:16"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T21:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T21:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T21:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T21"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-04T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-04-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2014-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05T20"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05"))
        assert_false(query_assertion(rec, d, " IN ", "2014"))

        # Date YYYY-MM-ddThh:mm:ss.ns
        d = "2015-05-05T20:15:30.123"
        assert_true(query_assertion(rec, d, " IN ", d))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05T20:15"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05T20"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05-05"))
        assert_true(query_assertion(rec, d, " IN ", "2015-05"))
        assert_true(query_assertion(rec, d, " IN ", "2015"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T20:15:30.124"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T20:15:31.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T20:15:31"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T20:16:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T20:16:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T20:16"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-05T21:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T21:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T21:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-05T21"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-04T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-04"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-04-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-05"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2014-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05T20"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-05"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05"))
        assert_false(query_assertion(rec, d, " IN ", "2014"))

        # Date YYYY-MM-dd
        d = "2015-01-01"
        assert_true(query_assertion(rec, d, " IN ", d))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-01-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-01-01T20"))
        assert_true(query_assertion(rec, d, " IN ", "2015-01-01"))
        assert_true(query_assertion(rec, d, " IN ", "2015-01"))
        assert_true(query_assertion(rec, d, " IN ", "2015"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-02-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-02-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-02-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-02-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-02-01"))
        assert_false(query_assertion(rec, d, " IN ", "2015-02"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2016-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2016-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2016-01-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2016-01-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2016-01-01"))
        assert_false(query_assertion(rec, d, " IN ", "2016-01"))
        assert_false(query_assertion(rec, d, " IN ", "2016"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2014-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2014-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2014-01-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2014-01-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2014-01-01"))
        assert_false(query_assertion(rec, d, " IN ", "2014-01"))
        assert_false(query_assertion(rec, d, " IN ", "2014"))

        # Date YYYY-MM
        d = "2015-05"
        assert_true(query_assertion(rec, d, " IN ", d))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01"))
        assert_true(query_assertion(rec, d, " IN ", "2015"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-06-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-06-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-06-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-06-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-06-01"))
        assert_false(query_assertion(rec, d, " IN ", "2015-06"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-04-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04-01"))
        assert_false(query_assertion(rec, d, " IN ", "2015-04"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2016-05-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05"))
        assert_false(query_assertion(rec, d, " IN ", "2016"))

        # Date YYYY
        d = "2015"
        assert_true(query_assertion(rec, d, " IN ", d))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2015-05-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05-01"))
        assert_false(query_assertion(rec, d, " IN ", "2015-05"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2016-05-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05-01"))
        assert_false(query_assertion(rec, d, " IN ", "2016-05"))
        assert_false(query_assertion(rec, d, " IN ", "2016"))
        assert_false(
            query_assertion(
                rec,
                d,
                " IN ",
                "2014-05-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-01T20:15:30"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-01T20:15"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-01T20"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05-01"))
        assert_false(query_assertion(rec, d, " IN ", "2014-05"))
        assert_false(query_assertion(rec, d, " IN ", "2014"))
    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass


def test_not_in_operator():
    try:
        rec = set_up_rec()

        # Date YYYY-MM-ddThh:mm:ss
        d = "2015-05-05T20:15:30"
        assert_false(query_assertion(rec, d, " NOT IN ", d))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30.123"))
        assert_false(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30.124"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:31.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15:31"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:16:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:16:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:16"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T21:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T21:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T21:15"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-06T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016"))

        # Date YYYY-MM-ddThh:mm:ss.ns
        d = "2015-05-05T20:15:30.123"
        assert_false(query_assertion(rec, d, " NOT IN ", d))
        assert_false(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30.123"))
        assert_false(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30.124"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:31.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15:31"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:16:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:16:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:16"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T21:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T21:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T21:15"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-06T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-06"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016"))

        # Date YYYY-MM-dd
        d = "2015-05-05"
        assert_false(query_assertion(rec, d, " NOT IN ", d))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-04-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014"))

        # Date YYYY-MM
        d = "2015-05"
        assert_false(query_assertion(rec, d, " NOT IN ", d))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-06"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-04-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-04"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014"))

        # Date YYYY
        d = "2015"
        assert_false(query_assertion(rec, d, " NOT IN ", d))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2015-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2015-05"))
        assert_false(query_assertion(rec, d, " NOT IN ", "2015"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2016"))
        assert_true(
            query_assertion(
                rec,
                d,
                " NOT IN ",
                "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014-05"))
        assert_true(query_assertion(rec, d, " NOT IN ", "2014"))
    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass


def test_eq_operator():
    try:
        rec = set_up_rec()

        # Date YYYY-MM-ddThh:mm:ss
        d = "2015-05-05T20:15:30"
        assert_true(query_assertion(rec, d, "=", d))
        assert_false(query_assertion(rec, d, "=", "2015-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "=", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "=", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "=", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "=", "2015-05-05"))
        assert_false(query_assertion(rec, d, "=", "2015-05"))
        assert_false(query_assertion(rec, d, "=", "2015"))

        # Date YYYY-MM-ddThh:mm:ss.ns
        d = "2015-05-05T20:15:30.123"
        assert_true(query_assertion(rec, d, "=", d))
        assert_true(query_assertion(rec, d, "=", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "=", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "=", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "=", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "=", "2015-05-05"))
        assert_false(query_assertion(rec, d, "=", "2015-05"))
        assert_false(query_assertion(rec, d, "=", "2015"))

        # Date YYYY-MM-dd
        d = "2015-01-01"
        assert_true(query_assertion(rec, d, "=", d))
        assert_false(query_assertion(rec, d, "=", "2015-01-02"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20"))
        assert_false(query_assertion(rec, d, "=", "2015-01"))
        assert_false(query_assertion(rec, d, "=", "2015"))

        # Date YYYY-MM
        d = "2015-01"
        assert_true(query_assertion(rec, d, "=", d))
        assert_false(query_assertion(rec, d, "=", "2015-02"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01"))
        assert_false(query_assertion(rec, d, "=", "2015"))

        # Date YYYY
        d = "2015"
        assert_true(query_assertion(rec, d, "=", d))
        assert_false(query_assertion(rec, d, "=", "2016"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20:15"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01T20"))
        assert_false(query_assertion(rec, d, "=", "2015-01-01"))
        assert_false(query_assertion(rec, d, "=", "2015-01"))

    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass


def test_neq_operator():
    try:
        rec = set_up_rec()

        # Date YYYY-MM-ddThh:mm:ss
        d = "2015-05-05T20:15:30"
        assert_false(query_assertion(rec, d, "!=", d))
        assert_true(query_assertion(rec, d, "!=", "2015-05-05T20:15:31"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05"))
        assert_false(query_assertion(rec, d, "!=", "2015-05"))
        assert_false(query_assertion(rec, d, "!=", "2015"))

        # Date YYYY-MM-ddThh:mm:ss.ns
        d = "2015-05-05T20:15:30.123"
        assert_false(query_assertion(rec, d, "!=", d))
        assert_true(query_assertion(rec, d, "!=", "2015-05-05T20:15:30.124"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "!=", "2015-05-05"))
        assert_false(query_assertion(rec, d, "!=", "2015-05"))
        assert_false(query_assertion(rec, d, "!=", "2015"))

        # Date YYYY-MM-dd
        d = "2015-01-01"
        assert_false(query_assertion(rec, d, "!=", d))
        assert_true(query_assertion(rec, d, "!=", "2015-01-02"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20"))
        assert_false(query_assertion(rec, d, "!=", "2015-01"))
        assert_false(query_assertion(rec, d, "!=", "2015"))

        # Date YYYY-MM
        d = "2015-01"
        assert_false(query_assertion(rec, d, "!=", d))
        assert_true(query_assertion(rec, d, "!=", "2015-02"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01"))
        assert_false(query_assertion(rec, d, "!=", "2015"))

        # Date YYYY
        d = "2015"
        assert_false(query_assertion(rec, d, "!=", d))
        assert_true(query_assertion(rec, d, "!=", "2016"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15:30.123"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15:30"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20:15"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01T20"))
        assert_false(query_assertion(rec, d, "!=", "2015-01-01"))
        assert_false(query_assertion(rec, d, "!=", "2015-01"))

    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass


def test_smaller_operator():
    try:
        rec = set_up_rec()

        # Date YYYY-MM-ddThh:mm:ss
        d = "2015-05-05T20:15:30"
        assert_false(query_assertion(rec, d, "<", d))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05"))
        assert_false(query_assertion(rec, d, "<", "2015-05"))
        assert_false(query_assertion(rec, d, "<", "2015"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.122"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.124"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:29.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:29"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:15:31.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:15:31"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:14:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:14:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:14"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:16:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:16:30"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:16"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21:15"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05"))
        assert_false(query_assertion(rec, d, "<", "2015-04"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05"))
        assert_true(query_assertion(rec, d, "<", "2015-06"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05"))
        assert_false(query_assertion(rec, d, "<", "2014-05"))
        assert_false(query_assertion(rec, d, "<", "2014"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05"))
        assert_true(query_assertion(rec, d, "<", "2016-05"))
        assert_true(query_assertion(rec, d, "<", "2016"))

        # Date YYYY-MM-ddThh:mm:ss.ns
        d = "2015-05-05T20:15:30.123"
        assert_false(query_assertion(rec, d, "<", d))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05"))
        assert_false(query_assertion(rec, d, "<", "2015-05"))
        assert_false(query_assertion(rec, d, "<", "2015"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.122"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:15:30.124"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:29.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:29"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:15:31.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:15:31"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:14:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:14:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:14"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:16:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:16:30"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T20:16"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T19"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21:15"))
        assert_true(query_assertion(rec, d, "<", "2015-05-05T21"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05"))
        assert_false(query_assertion(rec, d, "<", "2015-04"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05"))
        assert_true(query_assertion(rec, d, "<", "2015-06"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05"))
        assert_false(query_assertion(rec, d, "<", "2014-05"))
        assert_false(query_assertion(rec, d, "<", "2014"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05"))
        assert_true(query_assertion(rec, d, "<", "2016-05"))
        assert_true(query_assertion(rec, d, "<", "2016"))

        # Date YYYY-MM-dd
        d = "2015-05-05"
        assert_false(query_assertion(rec, d, "<", d))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05"))
        assert_false(query_assertion(rec, d, "<", "2015-05"))
        assert_false(query_assertion(rec, d, "<", "2015"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-04"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20:15"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06T20"))
        assert_true(query_assertion(rec, d, "<", "2015-05-06"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05"))
        assert_false(query_assertion(rec, d, "<", "2015-04"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05"))
        assert_true(query_assertion(rec, d, "<", "2015-06"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05"))
        assert_false(query_assertion(rec, d, "<", "2014-05"))
        assert_false(query_assertion(rec, d, "<", "2014"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05"))
        assert_true(query_assertion(rec, d, "<", "2016-05"))
        assert_true(query_assertion(rec, d, "<", "2016"))

        # Date YYYY-MM
        d = "2015-05"
        assert_false(query_assertion(rec, d, "<", d))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05"))
        assert_false(query_assertion(rec, d, "<", "2015-05"))
        assert_false(query_assertion(rec, d, "<", "2015"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-04-05"))
        assert_false(query_assertion(rec, d, "<", "2015-04"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05T20"))
        assert_true(query_assertion(rec, d, "<", "2015-06-05"))
        assert_true(query_assertion(rec, d, "<", "2015-06"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05"))
        assert_false(query_assertion(rec, d, "<", "2014-05"))
        assert_false(query_assertion(rec, d, "<", "2014"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05"))
        assert_true(query_assertion(rec, d, "<", "2016-05"))
        assert_true(query_assertion(rec, d, "<", "2016"))

        # Date YYYY
        d = "2015"
        assert_false(query_assertion(rec, d, "<", d))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2015-05-05"))
        assert_false(query_assertion(rec, d, "<", "2015-05"))
        assert_false(query_assertion(rec, d, "<", "2015"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20:15"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05T20"))
        assert_false(query_assertion(rec, d, "<", "2014-05-05"))
        assert_false(query_assertion(rec, d, "<", "2014-05"))
        assert_false(query_assertion(rec, d, "<", "2014"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20:15"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05T20"))
        assert_true(query_assertion(rec, d, "<", "2016-05-05"))
        assert_true(query_assertion(rec, d, "<", "2016-05"))
        assert_true(query_assertion(rec, d, "<", "2016"))

    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass


def test_greater_operator():
    try:
        rec = set_up_rec()
        # query with >
        # Date YYYY-MM-ddThh:mm:ss
        d = "2015-05-05T20:15:30"
        assert_false(query_assertion(rec, d, ">", d))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05"))
        assert_false(query_assertion(rec, d, ">", "2015-05"))
        assert_false(query_assertion(rec, d, ">", "2015"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.122"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.124"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:15:29.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:15:29"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:31.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:31"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:14:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:14:30"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:14"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:16:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:16:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:16"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19:15"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05"))
        assert_true(query_assertion(rec, d, ">", "2015-04"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05"))
        assert_false(query_assertion(rec, d, ">", "2015-06"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05"))
        assert_true(query_assertion(rec, d, ">", "2014-05"))
        assert_true(query_assertion(rec, d, ">", "2014"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05"))
        assert_false(query_assertion(rec, d, ">", "2016-05"))
        assert_false(query_assertion(rec, d, ">", "2016"))

        # Date YYYY-MM-ddThh:mm:ss.ns
        d = "2015-05-05T20:15:30.123"
        assert_false(query_assertion(rec, d, ">", d))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05"))
        assert_false(query_assertion(rec, d, ">", "2015-05"))
        assert_false(query_assertion(rec, d, ">", "2015"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:15:30.122"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.124"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:15:29.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:15:29"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:31.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:31"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:14:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:14:30"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T20:14"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:16:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:16:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:16"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19:15"))
        assert_true(query_assertion(rec, d, ">", "2015-05-05T19"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T21"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05"))
        assert_true(query_assertion(rec, d, ">", "2015-04"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05"))
        assert_false(query_assertion(rec, d, ">", "2015-06"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05"))
        assert_true(query_assertion(rec, d, ">", "2014-05"))
        assert_true(query_assertion(rec, d, ">", "2014"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05"))
        assert_false(query_assertion(rec, d, ">", "2016-05"))
        assert_false(query_assertion(rec, d, ">", "2016"))

        # Date YYYY-MM-dd
        d = "2015-05-05"
        assert_false(query_assertion(rec, d, ">", d))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05"))
        assert_false(query_assertion(rec, d, ">", "2015-05"))
        assert_false(query_assertion(rec, d, ">", "2015"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-06"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20:15"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04T20"))
        assert_true(query_assertion(rec, d, ">", "2015-05-04"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05"))
        assert_false(query_assertion(rec, d, ">", "2015-06"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05"))
        assert_true(query_assertion(rec, d, ">", "2015-04"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05"))
        assert_false(query_assertion(rec, d, ">", "2016-05"))
        assert_false(query_assertion(rec, d, ">", "2016"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05"))
        assert_true(query_assertion(rec, d, ">", "2014-05"))
        assert_true(query_assertion(rec, d, ">", "2014"))

        # Date YYYY-MM
        d = "2015-05"
        assert_false(query_assertion(rec, d, ">", d))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05"))
        assert_false(query_assertion(rec, d, ">", "2015-05"))
        assert_false(query_assertion(rec, d, ">", "2015"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-06-05"))
        assert_false(query_assertion(rec, d, ">", "2015-06"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05T20"))
        assert_true(query_assertion(rec, d, ">", "2015-04-05"))
        assert_true(query_assertion(rec, d, ">", "2015-04"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05"))
        assert_false(query_assertion(rec, d, ">", "2016-05"))
        assert_false(query_assertion(rec, d, ">", "2016"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05"))
        assert_true(query_assertion(rec, d, ">", "2014-05"))
        assert_true(query_assertion(rec, d, ">", "2014"))

        # Date YYYY
        d = "2015"
        assert_false(query_assertion(rec, d, ">", d))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2015-05-05"))
        assert_false(query_assertion(rec, d, ">", "2015-05"))
        assert_false(query_assertion(rec, d, ">", "2015"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30.123"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15:30"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20:15"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05T20"))
        assert_false(query_assertion(rec, d, ">", "2016-05-05"))
        assert_false(query_assertion(rec, d, ">", "2016-05"))
        assert_false(query_assertion(rec, d, ">", "2016"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30.123"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15:30"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20:15"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05T20"))
        assert_true(query_assertion(rec, d, ">", "2014-05-05"))
        assert_true(query_assertion(rec, d, ">", "2014-05"))
        assert_true(query_assertion(rec, d, ">", "2014"))

    finally:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass
