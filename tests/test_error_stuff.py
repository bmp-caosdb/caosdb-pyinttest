# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 19.02.2015.

@author: tf
"""
from caosdb.exceptions import EntityDoesNotExistError, UniqueNamesError,\
    TransactionError, EntityError, UnqualifiedPropertiesError,\
    EntityHasNoDatatypeError, UnqualifiedParentsError


def test_retrieval_no_exception_raised():
    import caosdb as h
    from nose.tools import assert_true, assert_false  # @UnresolvedImport

    p = h.Property(name="Non-ExsistentProperty").retrieve(unique=True,
                                                          raise_exception_on_error=False)
    assert_false(p.is_valid())
    assert_true(p.id is None or p.id < 0)


def test_retrieval_exception_raised():
    import caosdb as h
    from nose.tools import assert_true, assert_is_not_none, assert_equals  # @UnresolvedImport

    # special error: EntityDoesNotExistError
    try:
        h.Property(name="Non-ExistentProperty").retrieve(unique=True,
                                                         raise_exception_on_error=True)
        assert_true(False)
    except EntityDoesNotExistError as e:
        print("print(" + str(id(e)) + ")")
        print(e)
        assert_is_not_none(e.get_entity())

    # more general error: EntityError
    try:
        h.Property(name="Non-ExistentProperty").retrieve(unique=True,
                                                         raise_exception_on_error=True)
        assert_true(False)
    except EntityError as e:
        print(e)
        assert_is_not_none(e.get_entity())
        assert_equals('Non-ExistentProperty', e.get_entity().name)

    # most general error: TransactionError
    try:
        h.Property(name="Non-ExistentProperty").retrieve(unique=True,
                                                         raise_exception_on_error=True)
        assert_true(False)
    except EntityDoesNotExistError as e:
        print(e)
        assert_is_not_none(e.get_entities())
        print(e.get_entities())
        assert_is_not_none(e.get_entity())
        print(e.get_entity())
        assert_equals(1, len(e.get_entities()))
        assert_equals('Non-ExistentProperty', e.get_entities()[0].name)


def test_insertion_no_exception_raised():
    import caosdb as h
    from nose.tools import assert_false, assert_true  # @UnresolvedImport

    try:
        p = h.Property(
            name="NoTypeProperty").insert(
            raise_exception_on_error=False)
        assert_false(p.is_valid())
        assert_true(p.id is None or p.id < 0)
    finally:
        try:
            p.delete()
        except BaseException:
            pass


def test_insertion_exception_raised():
    import caosdb as h
    from nose.tools import assert_true  # @UnresolvedImport

    try:
        p = h.Property(name="NoTypeProperty")
        p.insert(raise_exception_on_error=True)
        assert_true(False)
    except EntityError as e:
        print(e)
    finally:
        try:
            p.delete()
        except BaseException:
            pass


def test_insertion_with_invalid_parents():
    import caosdb as h
    # @UnresolvedImport
    from nose.tools import assert_false, assert_true, assert_is_not_none, assert_equals

    try:
        p = h.Property(
            name="NoTypeProperty",
            datatype="Text").add_parent(
            id=-1)
        p.insert(raise_exception_on_error=True)
        assert_true(False)
    except EntityError as e:
        print(e)
        assert_true(isinstance(e, UnqualifiedParentsError))
        assert_is_not_none(e.get_entity())
        assert_equals(e.get_entity().name, p.name)
        assert_equals(e.get_entity().id, p.id)
        assert_true(e.get_entity().has_errors())
        assert_false(p.is_valid())
        assert_false(e.get_entity().is_valid())
        assert_is_not_none(e.get_entities())
    finally:
        try:
            p.delete()
        except BaseException:
            pass


def test_insertion_with_invalid_properties():
    import caosdb as h
    # @UnresolvedImport
    from nose.tools import assert_false, assert_true, assert_is_not_none, assert_equals

    try:
        try:
            h.execute_query("FIND NoTypeProperty").delete()
        except BaseException:
            pass
        p = h.Property(
            name="NoTypeProperty",
            datatype="Text").add_property(
            id=-1)
        p.insert(raise_exception_on_error=True)
        raise AssertionError(
            "This should raise an UnqualifiedPropertiesError.")
    except EntityError as e:
        assert_true(isinstance(e, UnqualifiedPropertiesError))
        assert_is_not_none(e.get_entity())
        assert_equals(e.get_entity().name, p.name)
        assert_true(e.get_entity().has_errors())
        assert_true(p.has_errors())
        assert_false(p.is_valid())
        assert_false(e.get_entity().is_valid())
    finally:
        try:
            p.delete()
        except BaseException:
            pass


def test_entity_does_not_exist():
    import caosdb as h
    # @UnresolvedImport
    from nose.tools import assert_true, assert_false, assert_equals, assert_is_not_none

    try:
        p1 = h.Property(
            name="Non-ExistentProperty1").retrieve(raise_exception_on_error=False)
        p2 = h.Property(
            name="Non-ExistentProperty2").retrieve(raise_exception_on_error=False)
        p3 = h.Property(
            name="Non-ExistentProperty3").retrieve(raise_exception_on_error=False)
        assert_false(p1.is_valid())
        assert_true(p1.id is None or p1.id < 0)
        assert_false(p2.is_valid())
        assert_true(p2.id is None or p2.id < 0)
        assert_false(p3.is_valid())
        assert_true(p3.id is None or p3.id < 0)

        pe = h.Property(name="ExistentProperty", datatype="text").insert()

        c = h.Container().extend(
            [
                h.Property(
                    name="Non-ExistentProperty1"),
                h.Property(
                    name="Non-ExistentProperty2"),
                h.Property(
                    name="Non-ExistentProperty3"),
                h.Property(
                    name="ExistentProperty")])

        try:
            c.retrieve()
        except EntityDoesNotExistError as e:
            assert_equals(3, len(e.get_entities()))
            for entity in e.get_entities():
                assert_is_not_none(entity.name)
                assert_true(entity.name.startswith("Non-ExistentProperty"))
    finally:
        try:
            pe.delete()
        except BaseException:
            pass


def test_insert_existent_entity():
    import caosdb as h
    # @UnresolvedImport
    from nose.tools import assert_true, assert_false, assert_equals, assert_is_not_none

    try:
        p1 = h.Property(
            name="Non-ExistentProperty1").retrieve(raise_exception_on_error=False)
        p2 = h.Property(
            name="Non-ExistentProperty2").retrieve(raise_exception_on_error=False)
        p3 = h.Property(
            name="Non-ExistentProperty3").retrieve(raise_exception_on_error=False)
        assert_false(p1.is_valid())
        assert_true(p1.id is None or p1.id < 0)
        assert_false(p2.is_valid())
        assert_true(p2.id is None or p2.id < 0)
        assert_false(p3.is_valid())
        assert_true(p3.id is None or p3.id < 0)

        pe = h.Property(name="ExistentProperty", datatype="text").insert()
        assert_true(pe.is_valid())

        c = h.Container().extend(
            [
                h.Property(
                    name="Non-ExistentProperty1",
                    datatype="text"),
                h.Property(
                    name="Non-ExistentProperty2",
                    datatype="text"),
                h.Property(
                    name="Non-ExistentProperty3",
                    datatype="text"),
                h.Property(
                    name="ExistentProperty",
                    datatype="text")])

        try:
            c.insert(unique=True)
        except UniqueNamesError as e:
            assert_equals(1, len(e.get_entities()))
            for entity in e.get_entities():
                assert_is_not_none(entity.name)
                assert_equals(pe.name, entity.name)

    finally:
        try:
            c.delete()
        except BaseException:
            pass
        try:
            pe.delete()
        except BaseException:
            pass
        try:
            p3.delete()
        except BaseException:
            pass
        try:
            p2.delete()
        except BaseException:
            pass
        try:
            p1.delete()
        except BaseException:
            pass


def test_double_insertion():
    import caosdb as h
    from nose.tools import assert_true, assert_equals, assert_is_not_none  # @UnresolvedImport

    c1 = h.Container()
    try:
        c1.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_error_stuff.py)",
                datatype='text'))
        c1.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_error_stuff.py)",
                datatype='double'))
        c1.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_error_stuff.py)",
                datatype='integer'))
        c1.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_error_stuff.py)",
                datatype='datetime'))

        c1.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_error_stuff.py)") .add_property(
                name='SimpleTextProperty') .add_property(
                name='SimpleDoubleProperty') .add_property(
                    name='SimpleIntegerProperty') .add_property(
                        name='SimpleDatetimeProperty'))

        c1.insert()

        c2 = h.Container()
        c2.append(
            h.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_error_stuff.py)",
                datatype='text'))
        c2.append(
            h.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_error_stuff.py)",
                datatype='double'))
        c2.append(
            h.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_error_stuff.py)",
                datatype='integer'))
        c2.append(
            h.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_error_stuff.py)",
                datatype='datetime'))

        c2.append(
            h.RecordType(
                name="SimpleRecordType",
                description="simple recordType (from test_error_stuff.py)") .add_property(
                name='SimpleTextProperty') .add_property(
                name='SimpleDoubleProperty') .add_property(
                    name='SimpleIntegerProperty') .add_property(
                        name='SimpleDatetimeProperty'))
        try:
            c2.insert()
        except TransactionError as te:
            assert_true(isinstance(te, EntityError))
            assert_true(isinstance(te, UniqueNamesError))
            assert_true(hasattr(te, 'get_errors'))
            assert_is_not_none(te.get_errors)
            assert_is_not_none(te.get_errors())
            assert_true(hasattr(te, 'get_error'))
            assert_is_not_none(te.get_error)
            assert_is_not_none(te.get_error())
            assert_true(hasattr(te, 'get_entities'))
            assert_is_not_none(te.get_entities)
            assert_is_not_none(te.get_entities())
            assert_equals(5, len(te.get_entities()))
            assert_true(hasattr(te, 'get_container'))
            assert_is_not_none(te.get_container)
            assert_is_not_none(te.get_container())
            assert_equals(c2, te.get_container())

    finally:
        try:
            c2.delete()
        except BaseException:
            pass
        try:
            c1.delete()
        except BaseException:
            pass
