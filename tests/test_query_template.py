# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 16.03.2017.

@author: tf
"""
import caosdb as db
from nose.tools import with_setup, assert_true, assert_is_none, assert_equals, assert_is_not_none, nottest, assert_raises  # @UnresolvedImport


def setup_module():
    try:
        db.administration._delete_user(name="test_user")
    except Exception as e:
        print(e)
    try:
        db.execute_query("FIND Test*").delete()
    except Exception as e:
        print(e)
    db.RecordType(name="TestPerson").insert()
    db.Property(name="TestSignature", datatype=db.TEXT).insert()
    db.Property(name="TestConductor", datatype="TestPerson").insert()
    db.Record(
        name="TestJohnDoe").add_parent(
        name="TestPerson").add_property(
            name="TestSignature",
        value="tjd").insert()
    db.Property(name="TestAnimal", datatype=db.TEXT).insert()
    db.Property(name="TestRoom", datatype=db.TEXT).insert()
    db.RecordType(
        name="TestExperiment").add_property(
        name="TestAnimal",
        importance=db.OBLIGATORY).add_property(
            name="TestRoom",
        importance=db.OBLIGATORY).insert()
    db.Record(
        name="TestRecord").add_parent(
        name="TestExperiment").add_property(
            name="TestAnimal",
            value="Pig").add_property(
                name="TestRoom",
                value="Lab13").add_property(
                    name="TestConductor",
        value="TestJohnDoe").insert()


def teardown_module():
    try:
        db.administration._delete_user(name="test_user")
    except Exception as e:
        print(e)
    try:
        db.execute_query("FIND Test*").delete()
    except Exception as e:
        print(e)


def setup():
    pass


def teardown():
    try:
        db.get_connection()._logout()
        db.configure_connection()
        db.get_connection()._login()
    except Exception as e:
        print(e)
    try:
        db.execute_query("FIND QUERYTEMPLATE Test*").delete()
    except Exception as e:
        print(e)


@with_setup(setup, teardown)
def test_insertion_success():
    return db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query="FIND RECORD Experiment WHICH HAS A animal=Pig").insert()


@with_setup(setup, teardown)
def test_insertion_failure_syntax():
    q = db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query="SDASDUASIUF")
    with assert_raises(db.TransactionError) as cm:
        q.insert()
    print(cm.exception)
    assert_equals(
        cm.exception.msg,
        "An error occured during the parsing of this query. Maybe you use a wrong syntax?")


@with_setup(setup, teardown)
def test_insertion_failure_count_query_not_allowed():
    q = db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query="COUNT something")
    with assert_raises(db.TransactionError) as cm:
        q.insert()
    assert_equals(
        cm.exception.msg,
        "QueryTemplates may not be defined by 'COUNT...' queries for consistency reasons.")


@with_setup(setup, teardown)
def test_insertion_failure_select_query_not_allowed():
    query_def = "SELECT TestAnimal FROM TestExperiment WHICH HAS A TestAnimal = Pig"
    q = db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query=query_def)
    with assert_raises(db.TransactionError) as cm:
        q.insert()
    assert_equals(
        cm.exception.msg,
        "QueryTemplates may not be defined by 'SELECT... FROM...' queries for consistency reasons.")


@with_setup(setup, teardown)
def test_deletion_success():
    eid = test_insertion_success().id
    q = db.QueryTemplate(id=eid).delete()
    assert_true(q.is_deleted())


@with_setup(setup, teardown)
def test_deletion_failure_non_existing():
    q = db.QueryTemplate(id="12342")
    with assert_raises(db.TransactionError) as cm:
        q.delete()
    assert_equals(cm.exception.msg, "Entity does not exist.")


@with_setup(setup, teardown)
def test_retrieve_success():
    test_insertion_success()
    q = db.QueryTemplate(name="TestQueryTemplate").retrieve(sync=False)
    assert_true(q.is_valid())
    assert_is_not_none(q.query)
    assert_equals(q.query, "FIND RECORD Experiment WHICH HAS A animal=Pig")


@with_setup(setup, teardown)
def test_retrieve_failure_non_existing():
    q = db.QueryTemplate(id="12342")
    with assert_raises(db.TransactionError) as cm:
        q.retrieve()
    assert_equals(cm.exception.msg, "Entity does not exist.")


@with_setup(setup, teardown)
def test_update_success():
    q = test_insertion_success()
    q.query = "FIND NewStuff"
    q.update()


@with_setup(setup, teardown)
def test_update_failure_syntax():
    q = test_insertion_success()
    q.query = "ashdjfkasjdf"
    with assert_raises(db.TransactionError) as cm:
        q.update()
    assert_equals(
        cm.exception.msg,
        "An error occured during the parsing of this query. Maybe you use a wrong syntax?")


@with_setup(setup, teardown)
def test_update_failure_count_query_not_allowed():
    q = test_insertion_success()
    q.query = "COUNT somethingNew"
    with assert_raises(db.TransactionError) as cm:
        q.update()
    assert_equals(
        cm.exception.msg,
        "QueryTemplates may not be defined by 'COUNT...' queries for consistency reasons.")


@with_setup(setup, teardown)
def test_update_failure_select_query_not_allowed():
    q = test_insertion_success()
    q.query = "SELECT TestAnimal FROM TestExperiment WHICH HAS A TestAnimal = Pig"
    with assert_raises(db.TransactionError) as cm:
        q.update()
    assert_equals(
        cm.exception.msg,
        "QueryTemplates may not be defined by 'SELECT... FROM...' queries for consistency reasons.")


@with_setup(setup, teardown)
def test_update_failure_non_existing():
    q = db.QueryTemplate(id="12342")
    q.query = "FIND NewStuff"
    with assert_raises(db.TransactionError) as cm:
        q.update()
    assert_equals(cm.exception.msg, "Entity does not exist.")


@with_setup(setup, teardown)
def test_retrieve_as_entity_success():
    q = test_insertion_success()
    e = db.Entity(id=q.id).retrieve()
    assert_equals(e.id, q.id)


@with_setup(setup, teardown)
def test_query_simple_find():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    r = db.execute_query(query_def, unique=True)
    assert_equals(r.name, "TestRecord")
    assert_equals(len(r.get_properties()), 3)
    db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()
    assert_equals(
        db.execute_query(
            "FIND QUERYTEMPLATE TestPigExperiment",
            unique=True).name,
        "TestPigExperiment")
    assert_equals(
        db.execute_query(
            "FIND QUERYTEMPLATE",
            unique=True).name,
        "TestPigExperiment")
    assert_equals(
        db.execute_query(
            "FIND TestPigExperiment",
            unique=True).name,
        "TestRecord")


@with_setup(setup, teardown)
def test_query_with_select_in_outer_query():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    r = db.execute_query(query_def, unique=True)
    assert_equals(r.name, "TestRecord")
    assert_equals(len(r.get_properties()), 3)
    db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()
    assert_equals(
        db.execute_query(
            "FIND QUERYTEMPLATE TestPigExperiment",
            unique=True).name,
        "TestPigExperiment")
    assert_equals(
        db.execute_query(
            "FIND QUERYTEMPLATE",
            unique=True).name,
        "TestPigExperiment")
    assert_equals(
        db.execute_query(
            "FIND TestPigExperiment",
            unique=True).name,
        "TestRecord")
    r = db.execute_query(
        "SELECT TestAnimal FROM TestPigExperiment",
        unique=True)
    assert_equals(r.name, "TestRecord")
    assert_equals(len(r.get_properties()), 1)


@with_setup(setup, teardown)
def test_query_with_other_filters():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal"
    db.QueryTemplate(name="TestAnimalExperiment", query=query_def).insert()

    r = db.execute_query("FIND TestAnimalExperiment")
    assert_equals(len(r), 2)

    r = db.execute_query(
        "FIND TestAnimalExperiment WHICH HAS A TestAnimal=Pig",
        unique=True)
    assert_equals(r.name, "TestRecord")


@with_setup(setup, teardown)
def test_query_simple_find_with_wildcard():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    db.QueryTemplate(name="TestAnimalExperiment", query=query_def).insert()

    r = db.execute_query("FIND TestAnimal*")
    assert_equals(len(r), 2)

    r = db.execute_query(
        "FIND TestAnimal* WHICH HAS A TestAnimal=Pig",
        unique=True)
    assert_equals(r.name, "TestRecord")


@with_setup(setup, teardown)
def test_query_select_from_with_wildcard():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    db.QueryTemplate(name="TestAnimalExperiment", query=query_def).insert()

    r = db.execute_query("SELECT TestAnimal FROM TestAnimal*")
    assert_equals(len(r), 2)
    assert_equals(len(r.get_entity_by_name("TestRecord").get_properties()), 1)
    assert_equals(len(r.get_entity_by_name("TestAnimal").get_properties()), 0)


@with_setup(setup, teardown)
def test_query_without_permission():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    qt = db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()

    db.administration._insert_user(
        name="test_user", password="secret_1q!Q", status="ACTIVE", email=None, entity=None)

    db.configure_connection(username="test_user", password="secret_1q!Q")

    r = db.execute_query(query_def, unique=True)
    assert_equals(r.name, "TestRecord")
    r = db.execute_query("FIND TestPigExperiment", unique=True)
    assert_equals(r.name, "TestRecord")

    db.get_connection()._logout()
    db.configure_connection()
    db.get_connection()._login()

    e = db.Entity(id=qt.id)
    e.retrieve_acl()
    assert_is_not_none(e.acl)
    e.deny(username="test_user", permission="RETRIEVE:ENTITY")
    e.update_acl()

    db.configure_connection(username="test_user", password="secret_1q!Q")

    r = db.execute_query(query_def, unique=True)
    assert_equals(r.name, "TestRecord")
    r = db.execute_query("FIND TestPigExperiment")
    assert_equals(len(r), 0)


@with_setup(setup, teardown)
def test_query_with_subquery_referenced_by():
    assert_equals(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY A TestExperiment AS A TestConductor",
            unique=True).name,
        "TestJohnDoe")
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal=Pig"
    assert_equals(db.execute_query(query_def, unique=True).name, "TestRecord")
    db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()

    assert_equals(
        db.execute_query(
            "FIND TestPigExperiment",
            unique=True).name,
        "TestRecord")

    assert_true(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestPigExperiment AS A TestConductor",
            unique=True).name,
        "TestJohnDoe")
    assert_true(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestPigExperiment",
            unique=True).name,
        "TestJohnDoe")
