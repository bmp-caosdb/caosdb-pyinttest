# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 13.01.2016.

@author: tf
"""

import caosdb as h
# @UnresolvedImport
from nose.tools import assert_true, assert_equals, assert_false, assert_raises
from caosdb.exceptions import EntityError


def test_property():
    try:
        try:
            h.execute_query("FIND Simple*").delete()
        except BaseException:
            pass
        p = h.Property(
            name="SimpleBooleanProperty",
            datatype=h.BOOLEAN).insert()
        assert_true(p.is_valid())
        assert_equals(p.datatype, h.BOOLEAN)

        p2 = h.Property(id=p.id).retrieve()
        assert_true(p2.is_valid)
        assert_equals(p2.datatype, h.BOOLEAN)
    finally:
        try:
            p.delete()
        except BaseException:
            pass


def test_recordType():
    try:
        p = h.Property(
            name="SimpleBooleanProperty",
            datatype=h.BOOLEAN).insert()
        assert_true(p.is_valid())

        rt = h.RecordType(name="SimpleRecordType").add_property(p).insert()
        assert_true(rt.is_valid())
        assert_equals(
            rt.get_property("SimpleBooleanProperty").datatype,
            h.BOOLEAN)

        rt2 = h.RecordType(id=rt.id).retrieve()
        assert_true(rt2.is_valid())
        assert_equals(
            rt2.get_property("SimpleBooleanProperty").datatype,
            h.BOOLEAN)
    finally:
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            p.delete()
        except BaseException:
            pass


def test_record():
    try:
        p = h.Property(
            name="SimpleBooleanProperty",
            datatype=h.BOOLEAN).insert()
        assert_true(p.is_valid())

        rt = h.RecordType(name="SimpleRecordType").add_property(p).insert()
        assert_true(rt.is_valid())

        rec1 = h.Record(name="SimpleRecord1").add_parent(
            rt).add_property(p, value="TRUE").insert()
        assert_true(rec1.is_valid())
        assert_equals(
            rec1.get_property("SimpleBooleanProperty").datatype,
            h.BOOLEAN)
        assert_equals(rec1.get_property("SimpleBooleanProperty").value, "TRUE")

        rec1c = h.Record(id=rec1.id).retrieve()
        assert_true(rec1c.is_valid())
        assert_equals(rec1c.get_property(
            "SimpleBooleanProperty").datatype, h.BOOLEAN)
        assert_equals(rec1c.get_property(
            "SimpleBooleanProperty").value, "TRUE")

        rec2 = h.Record(name="SimpleRecord2").add_parent(
            rt).add_property(p, value=True).insert()
        assert_true(rec2.is_valid())
        assert_equals(
            rec2.get_property("SimpleBooleanProperty").datatype,
            h.BOOLEAN)
        assert_equals(rec2.get_property("SimpleBooleanProperty").value, "TRUE")

        rec2c = h.Record(id=rec2.id).retrieve()
        assert_true(rec2c.is_valid())
        assert_equals(rec2c.get_property(
            "SimpleBooleanProperty").datatype, h.BOOLEAN)
        assert_equals(rec2c.get_property(
            "SimpleBooleanProperty").value, "TRUE")

        rec3 = h.Record(
            name="SimpleRecord3").add_parent(rt).add_property(
            p, value="BLABLA")
        assert_raises(EntityError, rec3.insert)

        assert_false(rec3.is_valid())
        assert_equals(
            rec3.get_property(
                "SimpleBooleanProperty").get_errors()[0].description,
            "Cannot parse value to boolean (either 'true' or 'false, "
            "ignoring case).")

    finally:
        try:
            rec3.delete()
        except BaseException:
            pass
        try:
            rec2.delete()
        except BaseException:
            pass
        try:
            rec1.delete()
        except BaseException:
            pass
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            p.delete()
        except BaseException:
            pass
