# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 11.10.2016.

@author: tf
"""
import caosdb as h

# @UnresolvedImport
from nose.tools import assert_true, assert_equals, assert_is_not_none, assert_is_none


def setup_module():
    print("SETUP")
    teardown_module()
    h.Property(name="TestPropertyOne", datatype=h.TEXT).insert()
    h.Property(
        name="TestPropertyTwo",
        description="Desc2",
        datatype=h.TEXT).insert()
    h.RecordType(
        name="TestRecordType",
        description="DescRecTy").add_property(
        name="TestPropertyOne").add_property(
            name="TestPropertyTwo").insert()


def teardown_module():
    print("TEARDOWN")
    try:
        h.execute_query("FIND Test*").delete()
    except BaseException:
        pass


def test_id1():
    p1 = h.execute_query("FIND TestPropertyOne", unique=True)
    assert_true(p1.is_valid())
    assert_is_not_none(p1.name)
    assert_is_not_none(p1.datatype)
    assert_is_none(p1.description)

    p1_c = h.execute_query("SELECT id FROM TestPropertyOne", unique=True)
    assert_true(p1_c.is_valid())
    assert_equals(p1_c.id, p1.id)
    assert_is_none(p1_c.name)
    assert_is_none(p1_c.datatype)
    assert_is_none(p1_c.description)


def test_id2():
    p2 = h.execute_query("FIND TestPropertyTwo", unique=True)
    assert_true(p2.is_valid())
    assert_is_not_none(p2.name)
    assert_is_not_none(p2.datatype)
    assert_is_not_none(p2.description)

    p2_c = h.execute_query("SELECT id FROM TestPropertyTwo", unique=True)
    assert_true(p2_c.is_valid())
    assert_equals(p2_c.id, p2.id)
    assert_is_none(p2_c.name)
    assert_is_none(p2_c.datatype)
    assert_is_none(p2_c.description)


def test_id3():
    p3s = h.execute_query("SELECT description FROM TestProperty*")
    assert_equals(len(p3s), 2)
    for e in p3s:
        assert_is_not_none(e.id)


def test_name1():
    p3s = h.execute_query("SELECT description FROM TestProperty*")
    assert_equals(len(p3s), 2)
    for e in p3s:
        assert_is_not_none(e.name)


def test_name2():
    p3s = h.execute_query("SELECT name FROM TestProperty*")
    assert_equals(len(p3s), 2)
    for e in p3s:
        assert_is_not_none(e.name)
        assert_is_none(e.description)


def test_multi1():
    p1 = h.execute_query(
        "SELECT id, name, description FROM TestPropertyOne",
        unique=True)
    assert_is_not_none(p1.id)
    assert_equals(p1.name, "TestPropertyOne")
    assert_is_none(p1.description)

    p2 = h.execute_query(
        "SELECT id, name, description FROM TestPropertyTwo",
        unique=True)
    assert_is_not_none(p2.id)
    assert_equals(p2.name, "TestPropertyTwo")
    assert_equals(p2.description, "Desc2")


def test_sub1():
    rt = h.execute_query("FIND TestRecordType", unique=True)
    assert_is_not_none(rt.id)
    assert_is_not_none(rt.name)
    assert_is_not_none(rt.get_property("TestPropertyOne"))

    rt = h.execute_query(
        "SELECT TestPropertyOne FROM TestRecordType",
        unique=True)
    assert_is_not_none(rt.id)
    assert_is_not_none(rt.name)
    assert_is_not_none(rt.get_property("TestPropertyOne"))


def test_sub2():
    rt = h.execute_query(
        "SELECT TestPropertyTwo.description FROM TestRecordType",
        unique=True)
    assert_is_not_none(rt.id)
    assert_is_not_none(rt.name)
    assert_is_not_none(rt.get_property("TestPropertyTwo"))
    assert_equals(rt.get_property("TestPropertyTwo").description, "Desc2")
    assert_is_none(rt.get_property("TestPropertyTwo").datatype)
