# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Tests for gitlab issues from #300-#399."""

from __future__ import unicode_literals, print_function
import caosdb as db
from nose.tools import with_setup, assert_raises as raiz, assert_equal as eq


def _delete_test_entities():
    try:
        db.execute_query("FIND Entity WITH id >= 100").delete()
    except Exception as e:
        print(e)


def setup_module():
    _delete_test_entities()


def teardown_module():
    _delete_test_entities()


@with_setup(_delete_test_entities, _delete_test_entities)
def test_ticket_350_insert_with_invalid_ref():
    c = db.Container()
    e = db.Entity(name="SomeName")
    rt1 = db.RecordType(name="RT1")
    rt2 = db.RecordType(name="RT2").add_property(name="RT1", value="SomeName")
    c.extend([e, rt1, rt2])
    with raiz(db.TransactionError) as cm:
        c.insert()
    eq(cm.exception.errors[0].msg, "There is no such role 'Entity'.")
    eq(cm.exception.errors[1].msg, "Entity has unqualified properties.")
