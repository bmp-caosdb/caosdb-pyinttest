#!/usr/bin/python2
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

# Testcase fuer deletion
# A. Schlemmer, 08/2014

from nose.tools import with_setup, assert_false, assert_true, assert_raises, assert_equals, assert_is_not_none  # @UnresolvedImport

import caosdb as h


def setup_module():
    old = h.execute_query("FIND ENTITY WITH ID > 100")
    if old:
        old.delete()


def setup161():
    h.RecordType(name="RT1").insert()
    h.RecordType(name="RT2").add_property("RT1").insert()
    h.RecordType(name="RT3").insert()


def teardown161():
    setup_module()


@with_setup(setup161, teardown161)
def test_delete_referencing_properties():
    rt3 = h.RecordType(name="RT3").retrieve()
    rt2 = h.RecordType(name="RT2").retrieve()
    rt3.add_property(
        id=rt2.id,
        name=rt2.name,
        datatype="RT2",
        inheritance=h.ALL)
    rt3.update()
    # and delete them
    h.execute_query("FIND ENTITY WITH ID > 100").delete()


def setup():
    try:
        h.execute_query("FIND Test*").delete()
    except Exception as e:
        print(e)


@with_setup(setup, setup)
def test_deletion():
    c = h.Container()
    c.append(
        h.Property(
            name="TestSimpleTextProperty",
            description="TestSimple text property (from test_deletion.py)",
            datatype='text'))
    c.append(
        h.Property(
            name="TestSimpleDoubleProperty",
            description="TestSimple double property (from test_deletion.py)",
            datatype='double'))
    c.append(
        h.Property(
            name="TestSimpleIntegerProperty",
            description="TestSimple integer property (from test_deletion.py)",
            datatype='integer'))
    c.append(
        h.Property(
            name="TestSimpleDatetimeProperty",
            description="TestSimple datetime property (from test_deletion.py)",
            datatype='datetime'))
    c.append(
        h.Property(
            name="TestSimpleFileProperty",
            description="TestSimple file property (from test_deletion.py)",
            datatype='file'))
    c.insert()

    d = h.Container()
    d.append(
        h.RecordType(
            name="TestSimpleRecordType",
            description="TestSimple recordType (from test_deletion.py)") .add_property(
            name='TestSimpleTextProperty') .add_property(
                name='TestSimpleDoubleProperty') .add_property(
                    name='TestSimpleIntegerProperty') .add_property(
                        name='TestSimpleDatetimeProperty') .add_property(
                            name='TestSimpleFileProperty'))

    d.append(
        h.Property(
            name="TestReferenceProperty",
            description='TestReference property (from test_deletion.py)',
            datatype="TestSimpleRecordType"))
    d.append(h.RecordType(name="TestComplexRecordType", description="TestComplex recordType with TestReferences (from test_deletion.py)")
             .add_property(name='TestReferenceProperty')  # first method
             # second method, doesn't need the TestReferenceProperty
             .add_property(name='TestSimpleRecordType')
             )

    d.insert()

    sr = (h.Record(name="TestRecord1").add_parent(name="TestSimpleRecordType")
          .add_property(name="TestSimpleTextProperty", value="Some Text")
          .add_property(name="TestSimpleDoubleProperty", value=3.14)
          .add_property(name="TestSimpleIntegerProperty", value=1337)
          )

    sr.insert()

    cr1 = (
        h.Record(
            name="TestRecord2").add_parent(
            name="TestComplexRecordType") .add_property(
                name="TestReferenceProperty",
            value=sr))

    cr1.insert()

    cr2 = (
        h.Record(
            name="TestRecord3").add_parent(
            name="TestComplexRecordType") .add_property(
                name="TestSimpleRecordType",
            value=sr))

    cr2.insert()
    assert_true(cr2.is_valid())
    assert_is_not_none(cr2.id)

    c.extend([cr1, sr, d])
    assert_raises(h.TransactionError, c.delete)
    assert_true(c.has_errors())
    assert_equals(int(c.get_errors()[0].code), 12)

    c.extend([cr2])
    print(c)
    c.delete()

    assert_false(c.has_errors())
