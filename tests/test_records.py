# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
from random import randint
from sys import maxsize as maxint
from nose.tools import nottest, assert_is_not_none, assert_equals, assert_true, assert_false
from caosdb import Record
from caosdb.exceptions import CaosDBException
import caosdb


def test_records():

    try:
        # insert test property
        pname = "PyTestProperty" + hex(randint(0, maxint))
        prop = caosdb.Property(
            name=pname,
            description="Desc PyTestProperty",
            datatype="Double",
            unit="meter")
        prop.insert()
        assert_is_not_none(prop.id)

        ''' insert test recordtype'''
        rtname = "PyTestRecordType" + hex(randint(0, maxint))
        recty = caosdb.RecordType(
            name=rtname, description="Desc PyTestRecordType")
        recty.add_property(name=pname, importance="obligatory")
        recty.insert()
        assert_is_not_none(recty.id)

        ''' insert test record '''
        rname = "PyTestRecord" + hex(randint(0, maxint))
        rec = caosdb.Record(name=rname, description="Desc PyTestRecord")
        rec.add_property(prop)

        ''' this should fail because the record has no RecordType yet...'''
        try:
            rec.insert()
            raise AssertionError("Should have raised a CaosDBException.")
        except CaosDBException:
            pass

        ''' add parent '''
        rec.add_parent(recty)
        rec.insert()
        assert_is_not_none(rec.id)

        ''' try to update via name'''
        rec_id = rec.id
        rec2 = caosdb.Record(name=rname)
        rec2.retrieve()
        assert_equals(rec_id, rec2.id)

        ''' assign value '''
        rec2.get_properties().get_by_name(pname).value = 2.5
        rec2.update()

        rec = caosdb.Record(id=rec_id).retrieve()
        assert_is_not_none(rec.name)

        c = caosdb.Container()
        c.append(rec)
        c.retrieve()

        try:
            recty.delete()
            raise AssertionError("Should have raised a CaosDBException.")
        except CaosDBException:
            pass

        c.delete()

        try:
            prop.delete()
            raise AssertionError("Should have raised a CaosDBException.")
        except CaosDBException:
            pass

        recty.delete()
        prop.delete()

    finally:
        try:
            c.delete()
        except BaseException:
            pass
        try:
            recty.delete()
        except BaseException:
            pass
        try:
            prop.delete()
        except BaseException:
            pass


def test_property_doublette1():
    # a record with three instances of the same property. two have values one
    # doesn't.

    try:

        """insert test property."""
        pname = "PyTestProperty" + hex(randint(0, maxint))
        prop = caosdb.Property(
            name=pname,
            description="Desc PyTestProperty",
            datatype="TEXT")
        prop.insert()
        assert_is_not_none(prop.id)

        ''' insert test recordtype'''
        rtname = "PyTestRecordType" + hex(randint(0, maxint))
        recty = caosdb.RecordType(
            name=rtname, description="Desc PyTestRecordType")
        recty.add_property(name=pname, importance="obligatory")
        recty.insert()
        assert_is_not_none(recty.id)

        ''' insert test record '''
        rname = "PyTestRecord" + hex(randint(0, maxint))
        rec = caosdb.Record(name=rname, description="Desc PyTestRecord")

        rec.add_property(id=prop.id, value="instance1")
        rec.add_property(id=prop.id, value="instance2")
        rec.add_property(id=prop.id)
        rec.add_parent(recty)

        rec.insert()
        assert_is_not_none(rec.id)
        assert_true(rec.is_valid())

        assert_equals(3, len(rec.get_properties()))
        found = {"instance1": False, "instance2": False, "empty": False}
        for p in rec.get_properties():
            print(p.value)
            if p.value is not None:
                found[p.value] = True
            else:
                found["empty"] = True
        for v in found.items():
            print(v)
            assert_true(v[1])

        rec2 = caosdb.Record(id=rec.id).retrieve()
        assert_is_not_none(rec2.id)
        assert_true(rec2.is_valid())

        assert_equals(3, len(rec2.get_properties()))
        found = {"instance1": False, "instance2": False, "empty": False}
        for p in rec2.get_properties():
            print(p.value)
            if p.value is not None:
                found[p.value] = True
            else:
                found["empty"] = True
        for v in found.items():
            print(v)
            assert_true(v[1])

    finally:
        try:
            rec.delete()
        except BaseException:
            pass
        try:
            recty.delete()
        except BaseException:
            pass
        try:
            prop.delete()
        except BaseException:
            pass


def test_property_doublette2():
    # a record with three instances of the same property. One has a value, the
    # other two don't.

    try:

        """insert test property."""
        pname = "PyTestProperty" + hex(randint(0, maxint))
        prop = caosdb.Property(
            name=pname,
            description="Desc PyTestProperty",
            datatype="TEXT")
        prop.insert()
        assert_is_not_none(prop.id)

        ''' insert test recordtype'''
        rtname = "PyTestRecordType" + hex(randint(0, maxint))
        recty = caosdb.RecordType(
            name=rtname, description="Desc PyTestRecordType")
        recty.add_property(name=pname, importance="obligatory")
        recty.insert()
        assert_is_not_none(recty.id)

        ''' insert test record '''
        rname = "PyTestRecord" + hex(randint(0, maxint))
        rec = caosdb.Record(name=rname, description="Desc PyTestRecord")

        rec.add_property(id=prop.id, value="instance1")
        rec.add_property(id=prop.id)
        rec.add_property(id=prop.id)
        rec.add_parent(recty)

        rec.insert()
        assert_is_not_none(rec.id)
        assert_true(rec.is_valid())

        assert_equals(3, len(rec.get_properties()))
        found = {"instance1": False, "empty": False}
        for p in rec.get_properties():
            print(p.value)
            if p.value is not None:
                found[p.value] = True
            else:
                found["empty"] = True
        for v in found.items():
            print(v)
            assert_true(v[1])

        rec2 = caosdb.Record(id=rec.id).retrieve()
        assert_is_not_none(rec2.id)
        assert_true(rec2.is_valid())

        assert_equals(3, len(rec2.get_properties()))
        found = {"instance1": False, "empty": False}
        for p in rec2.get_properties():
            print(p.value)
            if p.value is not None:
                found[p.value] = True
            else:
                found["empty"] = True
        for v in found.items():
            print(v)
            assert_true(v[1])

    finally:
        try:
            rec.delete()
        except BaseException:
            pass
        try:
            recty.delete()
        except BaseException:
            pass
        try:
            prop.delete()
        except BaseException:
            pass


def test_property_doublette3():
    # a record with three instances of the same property. One has a sub
    # property, the other two don't.

    try:

        """insert test property."""
        pname = "PyTestProperty" + hex(randint(0, maxint))
        prop = caosdb.Property(
            name=pname,
            description="Desc PyTestProperty",
            datatype="TEXT")
        prop.insert()
        assert_is_not_none(prop.id)

        ''' insert sub property'''
        subpname = "PyTestSubProperty" + hex(randint(0, maxint))
        subprop = caosdb.Property(
            name=subpname,
            description="Desc PyTestSubProperty",
            datatype="TEXT")
        subprop.insert()
        assert_is_not_none(subprop.id)

        ''' insert test recordtype'''
        rtname = "PyTestRecordType" + hex(randint(0, maxint))
        recty = caosdb.RecordType(
            name=rtname, description="Desc PyTestRecordType")
        recty.add_property(name=pname, importance="obligatory")
        recty.insert()
        assert_is_not_none(recty.id)

        ''' insert test record '''
        rname = "PyTestRecord" + hex(randint(0, maxint))
        rec = caosdb.Record(name=rname, description="Desc PyTestRecord")

        p1 = caosdb.Property(name=pname)
        p1.add_property(id=subprop.id, value="subinstance1")
        rec.add_property(p1)
        rec.add_property(id=prop.id)
        rec.add_property(id=prop.id)
        rec.add_parent(recty)

        rec.insert()
        assert_is_not_none(rec.id)
        assert_true(rec.is_valid())

        assert_equals(3, len(rec.get_properties()))
        found = 0
        for p in rec.get_properties():
            print(p)
            print(len(p.get_properties()))
            for subp in p.get_properties():
                print(p)
                print(subp)
                found += 1
        assert_equals(1, found)

        rec2 = caosdb.Record(id=rec.id).retrieve()
        assert_is_not_none(rec2.id)
        assert_true(rec2.is_valid())

        assert_equals(3, len(rec2.get_properties()))
        found = 0
        for p in rec2.get_properties():
            for subp in p.get_properties():
                print(subp)
                found += 1

        assert_equals(1, found)

    finally:
        try:
            rec.delete()
        except BaseException:
            pass
        try:
            recty.delete()
        except BaseException:
            pass
        try:
            subprop.delete()
        except BaseException:
            pass
        try:
            prop.delete()
        except BaseException:
            pass


def test_property_doublette4():
    # a record with three instances of the same property. One has a sub
    # property and a value, the second has only a value, the third has neither
    # of these.

    try:

        """insert test property."""
        pname = "PyTestProperty" + hex(randint(0, maxint))
        prop = caosdb.Property(
            name=pname,
            description="Desc PyTestProperty",
            datatype="TEXT")
        prop.insert()
        assert_is_not_none(prop.id)

        ''' insert sub property'''
        subpname = "PyTestSubProperty" + hex(randint(0, maxint))
        subprop = caosdb.Property(
            name=subpname,
            description="Desc PyTestSubProperty",
            datatype="TEXT")
        subprop.insert()
        assert_is_not_none(subprop.id)

        ''' insert test recordtype'''
        rtname = "PyTestRecordType" + hex(randint(0, maxint))
        recty = caosdb.RecordType(
            name=rtname, description="Desc PyTestRecordType")
        recty.add_property(name=pname, importance="obligatory")
        recty.insert()
        assert_is_not_none(recty.id)

        ''' insert test record '''
        rname = "PyTestRecord" + hex(randint(0, maxint))
        rec = caosdb.Record(name=rname, description="Desc PyTestRecord")

        p1 = caosdb.Property(name=pname)
        p1.add_property(id=subprop.id, value="subinstance1")
        rec.add_property(p1, value="instance1")
        rec.add_property(id=prop.id, value="instance2")
        rec.add_property(id=prop.id)
        rec.add_parent(recty)

        rec.insert()
        assert_is_not_none(rec.id)
        assert_true(rec.is_valid())

        assert_equals(3, len(rec.get_properties()))
        found = {"instance1": False, "instance2": False, "empty": False}
        for p in rec.get_properties():
            if p.value == "instance1":
                found["instance1"] = True
                assert_equals(1, len(p.get_properties()))
                assert_equals("subinstance1", p.get_properties()[0].value)
            elif p.value == "instance2":
                found["instance2"] = True
                assert_equals(0, len(p.get_properties()))
            else:
                found["empty"] = True
                assert_equals(0, len(p.get_properties()))
        for v in found.items():
            print(v)
            assert_true(v[1])

        rec2 = caosdb.Record(id=rec.id).retrieve()
        assert_is_not_none(rec2.id)
        assert_true(rec2.is_valid())

        assert_equals(3, len(rec2.get_properties()))
        found = {"instance1": False, "instance2": False, "empty": False}
        for p in rec2.get_properties():
            if p.value == "instance1":
                found["instance1"] = True
                assert_equals(1, len(p.get_properties()))
                assert_equals("subinstance1", p.get_properties()[0].value)
            elif p.value == "instance2":
                found["instance2"] = True
                assert_equals(0, len(p.get_properties()))
            else:
                found["empty"] = True
                assert_equals(0, len(p.get_properties()))
        for v in found.items():
            print(v)
            assert_true(v[1])

    finally:
        try:
            rec.delete()
        except BaseException:
            pass
        try:
            recty.delete()
        except BaseException:
            pass
        try:
            subprop.delete()
        except BaseException:
            pass
        try:
            prop.delete()
        except BaseException:
            pass


@nottest
def test_non_existent():
    rec = Record(id=466142163).retrieve(raise_exception_on_error=False)

    assert_false(rec.is_valid())

    found = False
    for m in rec.messages:
        if m.code == 101:
            found = True

    assert_true(found)
