Project migrated to https://gitlab.com/caosdb

[![pipeline status](https://gitlab.gwdg.de/bmp-caosdb/caosdb-pyinttest/badges/dev/pipeline.svg?job=codestyle)](https://gitlab.gwdg.de/bmp-caosdb/caosdb-pyinttest/commits/dev) codestyle

# Welcome

This is the **CaosDB Python Integration Tests** repository and a part of the
CaosDB project.


# Further Reading

Please refer to the [official gitlab repository of the CaosDB
project](https://gitlab.gwdg.de/bmp-caosdb/caosdb) for more information.

# License

Copyright (C) 2018 Research Group Biomedical Physics, Max Planck Institute for
Dynamics and Self-Organization Göttingen.

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE.md) (version 3 or later).

